let scripts = document.getElementsByTagName('script');
let currentScriptPath = scripts[scripts.length - 1].src;
let relativePathDirective = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1);
let relativePath = relativePathDirective.substring(0, relativePathDirective.lastIndexOf('/') + 1);


let angular = window.angular;
angular
	.module('superCrate')
	.factory('superCrateServices', superCrateServices);

superCrateServices.$inject = ['$http', '$rootScope', '$q', '$filter'];

function superCrateServices($http, $rootScope, $q, $filter) {
	let service = {};

	service.parseZipToAddress = parseZipToAddress;
	service.parseFullTAddress = parseFullTAddress;
	service.ContainsLocation = ContainsLocation;
	service.SetRequestAddress = SetRequestAddress;
	service.Calculate = Calculate;
	service.CreateRequest = CreateRequest;
	service.PrepareRequestArray = PrepareRequestArray;
	service.createInvoice = createInvoice;
	service.hashInvoice = hashInvoice;
	service.createPackageDescription = createPackageDescription;
	service.createPaymentInfo = createPaymentInfo;
	service.sendPayment = sendPayment;
	service.GetCardType = GetCardType;
	service.createSalesNote = createSalesNote;

	let i93;
	let i495;

	return service;

	function sendPayment(payment_info, nid, entity_type) {
		let data = payment_info;
		data.entity_id = nid,
			data.entity_type = entity_type;
		let deferred = $q.defer();
		return $http.post(config.serverUrl + 'server/payment', data).success(function (data) {
			deferred.resolve(data);
		}).error(function (error, status) {
			deferred.reject(error);
		}),
			deferred.promise;
	}

	//Apply Payment
	function createPaymentInfo(order, invoice) {

		let payment_info = {
			name: order.client_first_name + ' ' + order.client_last_name,
			phone: order.client_phone,
			is_invoice: true,
			invoice_id: invoice.id,
			email: order.client_email,
			zip_code: order.billing_zip,
			entity_id: invoice.entity_id,
			entity_type: 0,
			card_type: GetCardType(order.payment.credit_card),
			description: 'Payment for Order',
			credit_card: {
				card_number: order.payment.credit_card.replace(/ /g, ''),
				exp_date: order.payment.expmonth + '/' + order.payment.expyear,
				card_code: order.payment.cvv
			},
			payment_method: 'creditcard',
			payment_flag: 'Payment for Order',
			date: moment().format('MM/DD/YYYY'),
			created: moment().unix(),
			amount: invoice.total
		};


		return payment_info;

	}

	function GetCardType(number) {
		let re = RegExp('^4');
		return null != number.match(re) ? 'Visa' : (re = RegExp('^5[1-5]'),
			null != number.match(re) ? 'Mastercard' : (re = RegExp('^3[47]'),
				null != number.match(re) ? 'AMEX' : (re = RegExp(
					'^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)'),
					null != number.match(re) ? 'Discover' : (re = RegExp('^(4026|417500|4508|4844|491(3|7))'),
						null != number.match(re) ? 'Visa Electron' : ''))));
	}


	//Create Invoice

	function hashInvoice(hash) {
		let data = {};
		data.hash = hash;
		let deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_invoice/hash_invoice', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}


	function createInvoice(order, floorRates) {

		let invoice = {
			'charges': [],
			'client_name': order.client_first_name + ' ' + order.client_last_name,
			'phone': order.client_phone,
			'date': moment().format('MMMM DD, YYYY'),
			'email': order.client_email,
			'discount': 0,
			'flag': 1,
			'request_invoice': true,
			'id': order.nid,
			'tax': 0,
			'total': 0,
			'totalInvoice': 0,
			'uid': 0
		};

		//Convert package into charge
		invoice.charges.push({
			qty: 1,
			cost: order.package.rate,
			description: createPackageDescription(order),
			name: order.package.package_name + 'package'
		});
		//Add Extra Charge if Delivery
		if (angular.isDefined(order.extra_pickup_distance_charge)) {
			invoice.charges.push({
				qty: 1,
				cost: order.extra_pickup_distance_charge,
				description: 'Extra Distance Fee (Pickup)',
				name: 'Extra Distance Fee (Pickup)'
			});
		}
		if (angular.isDefined(order.extra_pickup_distance_charge)) {
			invoice.charges.push({
				qty: 1,
				cost: order.extra_pickup_distance_charge,
				description: 'Extra Distance Fee (Pickup)',
				name: 'Extra Distance Fee (Pickup)'
			});
		}
		// Extra Floor
		if (angular.isDefined(order.pickupFloor)) {
			invoice.charges.push({
				qty: 1,
				cost: floorRates[order.pickupFloor - 1].rate,
				description: 'Extra Charge for Floor (Pikcup)',
				name: 'Extra Charge for Floor (Pikcup)'
			});
		}
		if (angular.isDefined(order.dropoffFloor)) {
			invoice.charges.push({
				qty: 1,
				cost: floorRates[order.dropoffFloor - 1].rate,
				description: 'Extra Charge for Floor (Dropoff',
				name: 'Extra Charge for Floor (Pikcup)'
			});
		}

		//Add Extra Items and Charges
		if (Object.keys(order.extra_supplies).length) {
			angular.forEach(order.extra_supplies, function (suply, key) {
				invoice.charges.push({
					qty: suply.count,
					cost: suply.rate,
					description: suply.count + ' ' + suply.supply_name + '.' + suply.supply_info,
					name: suply.supply_name
				});
			});
		}
		// Create Invoice from Order


		return invoice;
	}

	function createPackageDescription(order) {

		let description = order.package.package_name + 'package.' + order.package.number_of_crates + 'crates included.' + order.package.rolling_carts + ' Rolling carts.'
			+ order.package.week_of_usage + ' weeks of usage';


		return description;
	}

	function createSalesNote(order, floorRates, total, client_note) {
		let note = '<b>Package:</b>' + order.package.package_name + ' ' + order.package.number_of_crates + ' crates included. <br>';
		note = note + ' ' + order.package.rolling_carts + ' rolling carts.' + order.package.week_of_usage + ' weeks of usage<br>';
		note = note + '<b>Drop off:</b>' + order.dropoffDate + ' at ' + order.dropoffLocation.full_adr + '. Apt:' + order.apt_from + ' .Estimated time ' + order.dropOffTime_array[order.dropoffTime].start + '-' + order.dropOffTime_array[order.dropoffTime].finish + '</div><br>';
		note = note + '<b>Pickup:</b>' + order.pickupDate + ' at ' + order.pickupLocation.full_adr + '. Apt:' + order.apt_to + '.Estimated time 3PM - 6PM</div><br>';

		if (!client_note) {
			if (order.need_moving) {
				note = note + '<b>Need Moving: YES!!</b><br>';
			}
			if (order.need_storage) {
				note = note + '<b>Need Storage: YES!!</b><br>';
			}
		}

		note = note + '<b>Receipt Details:<br>';
		note = note + '<b>Package:</b>' + order.package.package_name + ' ' + order.package.number_of_crates + ' crates: $' + order.package.rate + '<br>';

		//extra week
		if (order.extraweek) {
			note = note + '<b>Extra Week Charge:</b>' + order.extraweek_number + ' weeks: ' + '$' + order.package.extra_week_cost * order.extraweek_number + '<br>';
		}

		//extra Droppof
		if (order.extra_dropoff_distance_charge) {
			note = note + '<b>Extra Drop off Delivery:</b>: ' + '$' + order.extra_dropoff_distance_charge + '<br>';
		}

		// Floor charge
		if (order.dropoffFloor != '1') {
			note = note + '<b>Extra Floor Charge (Drop off)</b>: ' + '$' + floorRates[order.dropoffFloor - 1].rate + '<br>';
		}

		//Floor  Charge 2
		if (order.pickupFloor != '1') {
			note = note + '<b>Extra Floor Charge (Pick up)</b>: ' + '$' + floorRates[order.pickupFloor - 1].rate + '<br>';
		}

		if (angular.isDefined(order.extra_supplies_array)) {
			if (order.extra_supplies_array.length) {
				angular.forEach(order.extra_supplies_array, function (suply, key) {
					note = note + '<b>' + suply.supply_name + ' (' + suply.count + ')</b>:' + '$' + (suply.rate * suply.count).toFixed(
						2) + '<br>';
				});
			}
		}

		if (order.discount_amount) {
			note = note + '<b>Discount</b>: ' + '$' + order.discount_amount + '<br>';
		}

		note = note + '<b>Total: </b>' + '$' + total + '<br>';


		return note;
	}

	function ContainsLocation(geometry) {
		let ne = geometry.location; //bounds.getNorthEast();

		let ifI93 = false;
		let ifI495 = false;
		let ifextra = false;
		let ifmove = false;
		// IF SMALL CERCUL IT's FREE I93
		if (google.maps.geometry.poly.containsLocation(ne, i93) == true) {
			ifI93 = true;
		}

		if (google.maps.geometry.poly.containsLocation(ne, i495) == true) {
			ifI495 = true;
		}

		if (!ifI93 && ifI495) {
			// it's big circul we need to pay extra
			ifextra = true;
			ifmove = true;
		}
		// Small circul it's free
		if (ifI93) {
			ifmove = true;
		}

		return {
			ifmove,
			ifextra
		};

	}

	function PrepareRequestArray(request) {
		let move_request = {};

		if (angular.isUndefined(request.preferedTime)) {
			request.preferedTime = 1; // Any TiME
		}

		let start_time = getStartTime(request.preferedTime);

		if (angular.isUndefined(request.moveDate)) {
			request.moveDate = request.moveDateFormat;
		}

		request.moveDate = moment(request.moveDate).format('YYYY-MM-DD');

		if (request.serviceType != 7) {
			request.premise = '';
		}

		move_request.data = {
			status: 1,
			title: 'Move Request',
			uid: request.current_user.uid,
			field_useweighttype: '1',
			field_first_name: request.current_user.field_user_first_name,
			field_last_name: request.current_user.field_user_last_name,
			field_e_mail: request.current_user.mail,
			field_phone: request.current_user.field_primary_phone,
			field_additional_phone: request.current_user.field_user_additional_phone,
			field_approve: request.status,
			field_move_service_type: request.serviceType,
			field_date: {
				date: request.moveDate,
				time: '15:10:00'
			},
			field_size_of_move: request.moveSize,
			field_type_of_entrance_from: request.typeFrom,
			field_type_of_entrance_to_: request.typeTo,

			field_start_time: request.preferedTime,
			field_actual_start_time: start_time.start_time1,
			field_start_time_max: start_time.start_time2,

			field_price_per_hour: request.rate,
			field_movers_count: request.crew,
			field_distance: Math.round(request.distance),
			field_travel_time: request.travelTime,
			field_minimum_move_time: request.min_time,
			field_maximum_move_time: request.max_time,
			field_duration: request.duration,
			field_parser_provider_name: request.poll,

			field_extra_furnished_rooms: request.checkedRooms.rooms,
			field_moving_to: {
				country: 'US',
				administrative_area: request.addressTo.state,
				locality: request.addressTo.city,
				postal_code: request.addressTo.postal_code,
				thoroughfare: request.adrTo,
				premise: request.premise,
			},
			field_apt_from: request.aptFrom,
			field_apt_to: request.aptTo,
			field_moving_from: {
				country: 'US',
				administrative_area: request.addressFrom.state,
				locality: request.addressFrom.city,
				postal_code: request.addressFrom.postal_code,
				thoroughfare: request.adrFrom,
				premise: '',
			},
			field_long_distance_rate: request.ldrate,
			field_reservation_price: getReservationPrice(request),
			field_dummy_send_mail: request.dummyMail, // CHECK IF NEED SEND MAILS TO CLIENT
		};

		if (request.moveSize == 11) {
			move_request.data.field_extra_furnished_rooms = [];
			move_request.data.field_commercial_extra_rooms = request.field_commercial_extra_rooms;
		}

		if (angular.isDefined(request.double_travel_time)) {
			move_request.data.field_double_travel_time = request.double_travel_time;
		}

		move_request.account = {
			name: request.current_user.mail,
			mail: request.current_user.mail,
			fields: {
				field_user_first_name: request.current_user.field_user_first_name,
				field_user_last_name: request.current_user.field_user_last_name,
				field_user_additional_phone: request.current_user.field_user_additional_phone,
				field_primary_phone: request.current_user.field_primary_phone,
			}
		};

		return move_request;
	}

	function CreateRequest(request, callback) {
		let deferred = $q.defer();

		$http
			.post(config.serverUrl + 'server/move_request', request)
			.success(function (data, status, headers, config) {
				//data.success = true;
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				// data.success = false;
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function Calculate(request, callback) {
		let deferred = $q.defer();

		$http.get(config.serverUrl + 'services/session/token').success(function (data) {
			let token = data;

			$http.defaults.headers.common['X-CSRF-Token'] = unescape(encodeURIComponent(token));


			$http
				.post(config.serverUrl + 'server/front', request)
				.success(function (data, status, headers, config) {
					//data.success = true;
					deferred.resolve(data);
				})
				.error(function (data, status, headers, config) {
					// data.success = false;
					deferred.reject(data);
				});
		});


		return deferred.promise;
	}

	function SetRequestAddress(data, type) {
		if (type == 'zipFrom') {
			$rootScope.globals.request.addressFrom = data;
		}
		else {
			$rootScope.globals.request.addressTo = data;
		}
	}

	function parseFullTAddress(placeResult) {
		let city, state, country, postal_code, street, number;

		let arrAddress = placeResult['address_components'];

		angular.forEach(arrAddress, function (address_component, i) {
			if (address_component.types[0] == 'administrative_area_level_1') {// State
				if (address_component.short_name.length < 3) {
					state = address_component.short_name;
				}
			}

			if (address_component.types.indexOf('locality') > -1 || address_component.types.indexOf(
				'neighborhood') > -1 || address_component.types.indexOf('sublocality') > -1) {// locality type
				city = address_component.long_name;
			}

			if (address_component.types[0] == 'country') {// locality type
				country = address_component.short_name;
			}

			if (address_component.types[0] == 'postal_code') {// "postal_code"
				postal_code = address_component.short_name;
			}
			if (address_component.types[0] == 'route') {// "postal_code"
				street = address_component.short_name;
			}
			if (address_component.types[0] == 'street_number') {// "postal_code"
				number = address_component.short_name;
			}

		});


		let address = {
			'formatted': placeResult.formatted_address,
			'city': city,
			'state': state,
			'postal_code': postal_code,
			'country': country,
			'full_adr': number + ' ' + street + ',' + city + ', ' + state + ' ' + postal_code,
			'street': number + ' ' + street
		};

		return address;
	}


	function parseZipToAddress(arrAddress, type) {
		let city, state, country, postal_code;

		angular.forEach(arrAddress, function (address_component, i) {
			if (address_component.types[0] == 'administrative_area_level_1') {// State
				if (address_component.short_name.length < 3) {
					state = address_component.short_name;
				}
			}

			if (address_component.types.indexOf('locality') > -1 || address_component.types.indexOf(
				'neighborhood') > -1 || address_component.types.indexOf('sublocality') > -1) {// locality type
				city = address_component.long_name;
			}

			if (address_component.types[0] == 'country') {// locality type
				country = address_component.short_name;
			}

			if (address_component.types[0] == 'postal_code') {// "postal_code"
				postal_code = address_component.short_name;
			}

		});


		let address = {
			'city': city,
			'state': state,
			'postal_code': postal_code,
			'country': country,
			'full_adr': city + ', ' + state + ' ' + postal_code

		};

		return address;
	}
}

function getStartTime(start_time) {
	let start_time1 = '';
	let start_time2 = '';

	if (start_time == 1) {
		start_time1 = '8:00 AM';
		start_time2 = '8:00 PM';
	}

	if (start_time == 2) {
		start_time1 = '8:00 AM';
		start_time2 = '10:00 AM';
	}

	if (start_time == 3) {
		start_time1 = '12:00 PM';
		start_time2 = '3:00 PM';
	}

	if (start_time == 4) {
		start_time1 = '1:00 PM';
		start_time2 = '4:00 PM';
	}

	if (start_time == 5) {
		start_time1 = '3:00 PM';
		start_time2 = '7:00 PM';
	}

	return {
		start_time1: start_time1,
		start_time2: start_time2,
	};
}