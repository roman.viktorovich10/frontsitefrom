"use strict";
import moment from 'moment';

let angular = window.angular;
let $ = window.$;
angular
  .module("superCrate")
  .controller("superCrateHeaderController", superCrateHeaderController)
  .controller("superCrateFormController", superCrateFormController)
  .directive("superCrateForm", superCrateForm)
  .directive("superCrateHeader", superCrateHeader)
  .directive("extraServicesBox", extraServicesBox)
  .directive("paymentBox", paymentBox)
  .directive("paymentinfoBox", paymentinfoBox)
  .directive("zipcode", processZipCode)
  .directive("sfisemail", sfIsEmail)
  .directive("validatephone", validatePhone)
  .directive("ccCreditCard", ccCreditCard);

superCrateFormController.$inject = [
  "$scope",
  "$rootScope",
  "$timeout",
  "superCrateServices",
  "config",
  "Variables",
  "Apinator",
  "Timezone"
];

function superCrateFormController($scope, $rootScope, $timeout, superCrateServices, config, Variables, Apinator,
  Timezone) {

  $scope.isServiceChecked = [];
  $scope.promoCode__temp = "";
  $scope.DropOffDatePickerOptions = {
    min: moment().add(1, "d").toDate(),
    package: { extra_week_cost: 0 }
  };

  $scope.PickupDatePickerOptions = {
    min: "",
    disable: [],
    calendar: {},
    package: { extra_week_cost: 0 }
  };

  $scope.pickupDateValidate = false;
  $scope.firstStepDone = false;
  $scope.deliveryAddressValid = false;
  $scope.pickupAddressValid = false;
  $scope.secondStepDone = false;
  $scope.thirdStepDone = false;
  $scope.client_validation = false;
  $scope.final_validation = false;
  $scope.payment_step = false;
  $scope.threesteps = true;
  $scope.sizeChoosen = false;
  $scope.currentStep = "step1";
  $scope.nextStep = false;
  $scope.step2img = config.assets + "supercrate/assets/locationoff.png"; // Location
  $scope.step3img = config.assets + "supercrate/assets/scheduleoff.png"; // Schedule
  $scope.step4img = config.assets + "supercrate/assets/extrasuppliesoff.png"; // Extra Supplies
  $scope.step5img = config.assets + "supercrate/assets/additionaloff.png"; // Extra Services
  $scope.step6img = config.assets + "supercrate/assets/completeoff.png"; // Last Step
  $scope.currentStepDesription = "Select Your Package";
  $scope.loading = false;
  $scope.errorValidation = false;
  $scope.firstTimeSubmition = false;
  $scope.invoice_hash = 0;
  $scope.promoDiscount = 0;
  $scope.discountAmount = 0;

  $scope.timeWindows = [
    {
      start: 8,
      end: 12
    },
    {
      start: 12,
      end: 16
    },
    {
      start: 16,
      end: 20
    }
  ];

  $scope.timeWindowsFormatted = $scope.timeWindows.map(({ start, end }, index) =>
    ({
      key: index,
      value: `${moment().hours(start).format("hA")} - ${moment().hours(end).format("hA")}`
    })
  );

  $scope.validation_array = {
    "payment_month": false,
    "payment_year": false,
    "payment_card": false,
    "client_first_name": false,
    "client_last_name": false,
    "client_phone": false,
    "client_email": false,
    "card_number": false,
    "checkout_clicked": false,
    "cvv": false

  };

  $scope.order = {
    discount: {},
    client: {},
    services: {
      autoServices: [],
      customServices: []
    },
    packages: {
      mainPackage: {},
      additionalPackages: []
    },
    deliveryAddress: {
      floor: 0
    },
    pickupAddress: {
      floor: 0
    },
    totalWithDiscount: 0
  };

  $scope.$watch("order.deliveryAddress.address", onDeliveryAddressChanged);
  $scope.$watch("order.deliveryAddress.city", onDeliveryAddressChanged);
  $scope.$watch("order.deliveryAddress.postal", onDeliveryAddressChanged);
  $scope.$watch("order.deliveryAddress.state", onDeliveryAddressChanged);
  $scope.$watch("order.deliveryAddress.floor", onDeliveryAddressChanged);

  function onDeliveryAddressChanged(newVal, oldVal) {
    $scope.deliveryAddressValid = !!($scope.order.deliveryAddress.address && $scope.order.deliveryAddress.address.length);
    $scope.secondStepDone = $scope.deliveryAddressValid && $scope.pickupAddressValid;

    calculateRequestTotalDebounced();
  }

  $scope.$watch("order.pickupAddress.address", onPickupAddressChanged);
  $scope.$watch("order.pickupAddress.city", onPickupAddressChanged);
  $scope.$watch("order.pickupAddress.postal", onPickupAddressChanged);
  $scope.$watch("order.pickupAddress.state", onPickupAddressChanged);
  $scope.$watch("order.pickupAddress.floor", onPickupAddressChanged);

  function onPickupAddressChanged(newVal, oldVal) {
    $scope.pickupAddressValid = !!($scope.order.pickupAddress.address && $scope.order.pickupAddress.address.length);
    $scope.secondStepDone = $scope.deliveryAddressValid && $scope.pickupAddressValid;

    calculateRequestTotalDebounced();
  }

  $scope.stepsDesription = [
    {
      "id": "step1",
      "desription": "Select Your Package",
      "img": "Optional caption"
    },
    {
      "id": "step2",
      "desription": "Drop Off and Pick Up Location",
      "iconoff": config.assets + "supercrate/assets/locationoff.png",
      "iconon": config.assets + "supercrate/assets/locationon.png"
    },
    {
      "id": "step3",
      "desription": "Select Your Dates and Times",
      "iconoff": config.assets + "supercrate/assets/scheduleoff.png",
      "iconon": config.assets + "supercrate/assets/scheduleon.png"
    }
    ,
    {
      "id": "step4",
      "desription": "Select Supplies(optional)",
      "iconoff": config.assets + "supercrate/assets/extrasuppliesoff.png",
      "iconon": config.assets + "supercrate/assets/extrasupplieson.png"
    },

    {
      "id": "step5",
      "desription": "Additional services",
      "iconoff": config.assets + "supercrate/assets/additionaloff.png",
      "iconon": config.assets + "supercrate/assets/additionalon.png"
    }
    ,
    {
      "id": "step6",
      "desription": "Complete Your Order",
      "iconoff": config.assets + "supercrate/assets/completeoff.png",
      "iconon": config.assets + "supercrate/assets/completeon.png"
    }
  ];


  $scope.step1 = true;
  $scope.step2 = false;
  $scope.step3 = false;
  $scope.step4 = false;
  $scope.step5 = false;
  $scope.step6 = false;
  $scope.mobile_summary_step = true;

  $scope.validateDate = false;
  $scope.validateTime = false;
  $scope.validatepickupTime = false;
  let twoDaysMs = 86400000;
  let createdRequest = {};


  Variables.initialPromise.then(res => {
    $scope.packages = Variables.Variables.packagesInfo.packages;

    $scope.floorRates = (["0"].concat(
      _.merge([0, 0, 0, 0, 0], Variables.Variables.servicesInfo.systemServices.floorService))).map(
      (serviceInfo, index) => ({
        index,
        name: index ? `${index + 1} - flight of stairs ($${serviceInfo} extra)`
                    : "Ground/Elevator - (No extra charge)"
      })
    );

    $scope.extra_supplies = _.cloneDeep(Variables.Variables.packagesInfo.supplies);
  });

  function initFlexSliders() {
    $(".flexslider").flexslider({
      animation: "slide",
      //  itemWidth: 300,
      //  itemMargin: 10,
      //   minItems: 2,
      //  maxItems: 4
      slideshow: false,
      animationLoop: false,
      controlsContainer: ".the_slider .contain"
    });
  }

  function calculateRequestTotal() {
    Apinator.calculateRequest($scope.order)
      .then(res => {
        if (!res.isOldPromise) {
          $scope.order = res;
          $scope.$apply();
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  let calculateRequestTotalDebounced = _.debounce(calculateRequestTotal, 300, {
    leading: true,
    trailing: true
  });

  $scope.sendMessagePickup = function(msg) {
    $scope.pickup_msg = msg || "";
    $scope.pickup_err = "";
  };

  $scope.sendMessageDelivery = function(msg) {
    $scope.dropoff_msg = msg || "";
    $scope.dropoff_err = "";
  };

  $scope.sendErrorPickup = function(msg) {
    $scope.pickup_err = msg || "";
    $scope.pickup_msg = "";
  };

  $scope.sendErrorDelivery = function(msg) {
    $scope.dropoff_err = msg || "";
    $scope.dropoff_msg = "";
  };

  $scope.add_supply = function(index) {
    $scope.extra_supplies[index].quantity++;
    if ($scope.extra_supplies[index].quantity > 0) {
      let foundSupply = _.findIndex($scope.order.packages.additionalPackages,
        { name: $scope.extra_supplies[index].name });
      if (foundSupply === -1) {
        $scope.order.packages.additionalPackages.push($scope.extra_supplies[index]);
      } else {
        $scope.order.packages.additionalPackages[foundSupply] = $scope.extra_supplies[index];
      }
    }
    calculateRequestTotalDebounced();
  };

  $scope.remove_supply = function(index) {
    $scope.extra_supplies[index].quantity--;
    if ($scope.extra_supplies[index].quantity < 0) $scope.extra_supplies[index].quantity = 0;

    let foundSupply = _.findIndex($scope.order.packages.additionalPackages,
      { name: $scope.extra_supplies[index].name });
    if (foundSupply !== -1) {
      if ($scope.extra_supplies[index].quantity === 0) {
        $scope.order.packages.additionalPackages.splice(foundSupply, 1);
      } else {
        $scope.order.packages.additionalPackages[foundSupply] = $scope.extra_supplies[index];
        $scope.order.packages.additionalPackages[foundSupply] = $scope.extra_supplies[index];
      }
    }

    calculateRequestTotalDebounced();
  };

  $scope.choosePackage = function(packageIndex) {
    $scope.firstStepDone = true;
    $scope.showStep("step4");

    $scope.packages.forEach((value, index) => {
      $scope.packages[index].chosen = index === packageIndex;
    });

    $scope.order.packages.mainPackage = $scope.packages[packageIndex];
    $scope.order.packages.mainPackage.quantity = 1;
    $scope.PickupDatePickerOptions.package.extra_week_cost = $scope.order.packages.mainPackage.extraUsageRate;

    calculateRequestTotalDebounced();
  };

  $scope.chooseExtraService = function(service) {
    let foundService = _.findIndex($scope.order.services.customServices, { name: service });
    if (foundService !== -1) {
      $scope.order.services.customServices.splice(foundService, 1);
      $scope.isServiceChecked[service] = false;
    } else {
      let serviceInSettings = _.find(Variables.Variables.servicesInfo.customServices, { name: service });
      if (serviceInSettings) {
        serviceInSettings.quantity = 1;
        $scope.order.services.customServices.push(serviceInSettings);
      }
      $scope.isServiceChecked[service] = true;
    }

    calculateRequestTotalDebounced();
  };

  $scope.validatePayment = function(type) {

    let current_year = moment().format("YY");

    if (type == "month") {
      return angular.isDefined($scope.payment.expmonth)
        && $scope.payment.expmonth > 0
        && $scope.payment.expmonth <= 12;
    }

    if (type == "year") {
      return angular.isDefined($scope.payment.expyear)
        && $scope.payment.expyear >= current_year;
    }
    if (type == "card") {
      return angular.isDefined($scope.payment.credit_card)
        && superCrateServices.GetCardType($scope.payment.credit_card);
    }
    if (type == "cvv") {
      if (angular.isDefined($scope.payment.credit_card) &&
        angular.isDefined($scope.payment.cvv)) {
        let card_type = superCrateServices.GetCardType($scope.payment.credit_card);

        if (card_type == "AMEX" && $scope.payment.cvv.length == 4) {
          return true;
        } else if (card_type != "AMEX" && $scope.payment.cvv.length == 3) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  function notEmpty(value) {
    return value && value.length;
  }

  $scope.clientValidation = function() {
    $scope.validation_array.client_first_name = notEmpty($scope.order.client.firstName);
    $scope.validation_array.client_last_name = notEmpty($scope.order.client.lastName);
    $scope.validation_array.client_phone = notEmpty($scope.order.client.email);
    $scope.validation_array.client_email = notEmpty($scope.order.client.phone);

    $scope.client_validation = $scope.validation_array.client_first_name
      && $scope.validation_array.client_last_name
      && $scope.validation_array.client_phone
      && $scope.validation_array.client_email;
  };

  $scope.finalValidation = function() {
    let final_validation = false;

    //check_fio and payments
    $scope.validation_array.payment_month = $scope.validatePayment("month");
    $scope.validation_array.payment_year = $scope.validatePayment("year");
    $scope.validation_array.card_number = $scope.validatePayment("card");
    $scope.validation_array.cvv = $scope.validatePayment("cvv");
    // var firstName ,lastname, email, numbverc, css exist;


    if ($scope.validation_array.payment_month
      && $scope.validation_array.payment_year &&
      $scope.validation_array.card_number && $scope.validation_array.client_first_name &&
      $scope.validation_array.client_last_name
    ) {
      final_validation = true;
    }

    $scope.final_validation = final_validation;

    return final_validation;

  };

  function fullAddressRow(value) {
    if (!value) return "";
    let result = "";
    if (value.address) result += value.address;
    if (value.city) result += ", " + value.city;
    if (value.state) result += ", " + value.state;
    if (value.postal) result += ", " + value.postal;
    return result;
  }

  $scope.getFullDeliveryAddress = function() {
    return fullAddressRow($scope.order.deliveryAddress);
  };

  $scope.getFullPickupAddress = function() {
    return fullAddressRow($scope.order.pickupAddress);
  };

  $scope.applypromocode = function() {
    Apinator.GATE("checkPromoCode", { promoCode: $scope.promoCode__temp })
      .then(res => {
        if (res.promoConfirmed) {
          $scope.order.discount.promoCode = $scope.promoCode__temp;
          calculateRequestTotalDebounced();
        } else {
          $scope.promoCode__temp = "";
          swal({
            title: "The code has been entered incorrectly.",
            text: "Whoops! The code are case sensitive so enter it exactly as it is written and try again.",
            icon: "warning",
            button: "Ok"
          });
        }
      })
      .catch(err => {
        console.error(err);
      });
  };

  function validateSteps(step) {

    var validation = false;
    $scope.validate_msg = "";

    switch (step) {
      case "step1":
        if ($scope.firstStepDone) {
          validation = true;
        }
        break;
      case "step2":
        if ($scope.firstStepDone) {
          validation = true;
        }
        break;
      case "step3":
        $scope.step3 = false;
        if ($scope.firstStepDone && $scope.secondStepDone) {
          validation = true;
        }
        break;
      case "step4":
        $scope.step4 = false;
        if ($scope.firstStepDone) {
          validation = true;
        }
        // if ($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone) {
        //   validation = true;
        // }
        break;
      case "step5":
        $scope.step5 = false;
        if ($scope.firstStepDone && $scope.thirdStepDone) {
          validation = true;
        }
        break;
      case "step6":
        $scope.step6 = false;
        if ($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone) {
          validation = true;
        }
        break;
      case "stepSuccess":
        if ($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone) {
          validation = true;
        }
        break;

      default:

    }

    $scope.errorValidation = !validation;

    return validation;
  }

  const STEP_NAMES = {
    'step1': 'step1',
    'step2': 'step4',
    'step3': 'step2',
    'step4': 'step3',
    'step5': 'step6',
    'step6': 'step6',
  };

  $scope.showStepByProgressBar = function(stepName) {
    $scope.showStep(STEP_NAMES[stepName]);
  };

  $scope.showStep = function(new_step) {

    if (new_step != $scope.currentStep) {


      if (validateSteps(new_step)) {
        nextStep(new_step);
        hideCurrentStep(new_step);
        showStepDesription(new_step);
      }
    }

  };

  $scope.deliveryDateChange = function() {
    if (angular.isDefined($scope.order.deliveryDate_temp)) {

      let timezonedMoment = Timezone.moveDateToMomentTimezone(new Date($scope.order.deliveryDate_temp));

      $scope.deliveryDateValid = true;
      $scope.order.deliveryDate = timezonedMoment.unix();

      let StorageDay = moment(timezonedMoment).add(2, 'days');
      $scope.calendar = {};
      let year = timezonedMoment.format("YYYY");
      $scope.calendar[year] = {};

      for (let i = 0; i < 14; i++) {

        let date = timezonedMoment.add(1, "days").format("YYYY-MM-DD");
        $scope.calendar[year][date] = "1";

      }

      for (let i = 0; i < 305; i++) {

        let date = timezonedMoment.add(1, "days").format("YYYY-MM-DD");
        $scope.calendar[year][date] = "2";
      }
      $scope.PickupDatePickerOptions.calendar = $scope.calendar;
      $scope.PickupDatePickerOptions.min = StorageDay;
      // $scope.PickupDatePickerOptions.disable = [1];
      // $scope.order.pickupDate = moment($scope.order.dropoffDate).add(2, 'w').format('MMMM DD,YYYY');


      // $scope.note_text = "We add 2 weeks to Pickup Date. You can change it if you want."

      // Create Calendar
    } else {
      $scope.deliveryDateValid = false;
    }

    validateThirdStep();
    calculateRequestTotalDebounced();
  };

  $scope.pickupdateChange = function() {
    if (angular.isDefined($scope.order.pickupDate_temp)) {
      let timezonedMoment = Timezone.moveDateToMomentTimezone(new Date($scope.order.pickupDate_temp));
      $scope.order.pickupDate = timezonedMoment.unix();
      $scope.pickupDateValid = true;
    } else {
      $scope.pickupDateValid = false;
    }
    validateThirdStep();
    calculateRequestTotalDebounced();
  };

  $scope.deliveryTimeWindowChange = function() {
    $scope.deliveryTimeWindowValid = angular.isDefined($scope.order.deliveryTimeWindow_temp);
    validateThirdStep();
  };

  $scope.pickupTimeWindowChange = function() {
    $scope.pickupTimeWindowValid = angular.isDefined($scope.order.pickupTimeWindow_temp);
    validateThirdStep();
  };

  function validateThirdStep() {
    $scope.thirdStepDone = $scope.deliveryDateValid
      && $scope.pickupDateValid
      && $scope.deliveryTimeWindowValid
      && $scope.pickupTimeWindowValid;
  }

  $scope.deliveryFloorChanged = function() {
  };

  $scope.pickupFloorChanged = function() {
  };

  // Check if step is complete, if true color progress bar in green if even not current step
  $scope.isCompleteStep = function(step) {

    switch (step) {
      case "step1":
        $scope.step1 = false;
        break;
      case "step2":
        $scope.step2 = false;
        break;
      case "step3":
        $scope.step3 = false;
        break;
      case "step4":
        $scope.step4 = false;
        break;
      case "step5":
        $scope.step5 = false;
        break;
      case "step6":
        $scope.step6 = false;
        break;

      default:

    }

  };

  function showStepDesription(step) {

    angular.forEach($scope.stepsDesription, function(value, key) {

      if (angular.equals(value.id, step)) {
        $scope.currentStepDesription = value.desription;
      }

    });

  }

  $scope.showPayment = function() {

    $scope.payment_step = true;

  };

  // CHECKOUT FUNCTION //
  function prepareRequestForCreating(request) {
    let result = _.cloneDeep(request);

    result.deliveryTimeWindowStart = moment
      .unix(result.deliveryDate)
      .hour($scope.timeWindows[result.deliveryTimeWindow_temp].start)
      .unix();
    result.deliveryTimeWindowEnd = moment
      .unix(result.deliveryDate)
      .hour($scope.timeWindows[result.deliveryTimeWindow_temp].end)
      .unix();

    result.pickupTimeWindowStart = moment
      .unix(result.pickupDate)
      .hour($scope.timeWindows[result.pickupTimeWindow_temp].start)
      .unix();
    result.pickupTimeWindowEnd = moment
      .unix(result.pickupDate)
      .hour($scope.timeWindows[result.pickupTimeWindow_temp].end)
      .unix();

    delete result.deliveryTimeWindow_temp;
    delete result.pickupTimeWindow_temp;
    delete result.deliveryDate_temp;
    delete result.pickupDate_temp;

    return result;
  }

  $scope.submitOrder = function() {

    $scope.validation_array.checkout_clicked = true;
    $scope.clientValidation();

    if ($scope.client_validation) {
      let orderForBack = prepareRequestForCreating($scope.order);

      Apinator.GATE("createUserFromFrontSite", orderForBack.client)
        .catch((reason) => {
          $scope.loading = false;
          swal({
            title: "Error",
            text: "Problem with creating user. Please call in the office",
            icon: "warning",
            button: "Ok"
          });
          throw reason;
        })
        .then(user => {
          $scope.invoice_hash = user.hashLogin;
          orderForBack.client = user.id;
          return Apinator.GET(`auth/hashLogin`, { hash: $scope.invoice_hash });
        })
        .catch((reason) => {
          $scope.loading = false;
          swal({
            title: "Error",
            text: "Problem with login. Please call in the office",
            icon: "warning",
            button: "Ok"
          });
          throw reason;
        })
        .then(res => {
          return Apinator.GATE("createRequest", orderForBack);
        })
        .catch((reason) => {
          $scope.loading = false;
          swal({
            title: "Error",
            text: "Problem with creating order. Please call in the office",
            icon: "warning",
            button: "Ok"
          });
          throw reason;
        })
        .then((data) => {
          $scope.showPayment();
          $scope.firstTimeSubmition = true;
          $scope.$apply();
          createdRequest = data;
          //SHOW PAYMENT WINDOW
        })
        .catch((reason) => {
          console.error(reason);
        });
    }
  };

  $scope.checkout = function() {

    $scope.validation_array.checkout_clicked = true;
    let finalValidation = $scope.finalValidation();

    if (finalValidation) {

      if ($scope.firstTimeSubmition) {
        $scope.loading = true;
        Apinator.GATE("createReceipt", {
          user: {
            firstName: createdRequest.client.firstName,
            lastName: createdRequest.client.lastName,
            email: createdRequest.client.email
          },
          cardNumber: $scope.payment.credit_card,
          expirationDate: $scope.payment.expmonth + "/" + $scope.payment.expyear,
          cardCode: $scope.payment.cvv,
          zip: $scope.payment.billing_zip,
          description: "",
          amount: createdRequest.balance,
          entityId: createdRequest.id,
          type: 1 //CREDIT_CARD
        })
          .then((response) => {
            $scope.loading = false;

            if (!response.status || response.status !== 200) {
              swal({
                title: "Error",
                text: response.errorMessage,
                icon: "warning",
                button: "Ok"
              });
              $scope.loading = false;
            }
            else {
              $scope.showStep("stepSuccess");
              window.location = "/account/#/request/" + createdRequest.id;
            }
            $scope.$apply();
          })
          .catch((reason) => {
            $scope.loading = false;
            swal({
              title: "Error",
              text: reason.errorMessage,
              icon: "warning",
              button: "Ok"
            });
            $scope.$apply();
            // show payment dialog agagin
          });
      }

    }


  };

  $scope.summarytoggle = function() {
    $scope.mobile_summary_step = !$scope.mobile_summary_step;
  };


  $scope.toggleImage = function(step) {

    switch (step) {
      case "step1":

        $scope.step2img = config.assets + "supercrate/assets/locationoff.png";
        $scope.step3img = $scope.stepsDesription[2].iconoff;
        $scope.step4img = $scope.stepsDesription[3].iconoff;
        $scope.step5img = $scope.stepsDesription[4].iconoff;
        $scope.step6img = $scope.stepsDesription[5].iconoff;

        break;
      case "step2":
        if ($scope.step2) {
          $scope.step2img = config.assets + "supercrate/assets/locationon.png";
        }
        else {
          $scope.step2img = config.assets + "supercrate/assets/locationoff.png";
        }

        $scope.step3img = $scope.stepsDesription[2].iconoff;
        $scope.step4img = $scope.stepsDesription[3].iconoff;
        $scope.step5img = $scope.stepsDesription[4].iconoff;
        $scope.step6img = $scope.stepsDesription[5].iconoff;

        break;
      case "step3":
        if ($scope.step3) {
          $scope.step3img = $scope.stepsDesription[2].iconon;
        }
        else {
          $scope.step3img = $scope.stepsDesription[2].iconoff;
        }

        $scope.step4img = $scope.stepsDesription[3].iconoff;
        $scope.step5img = $scope.stepsDesription[4].iconoff;
        $scope.step6img = $scope.stepsDesription[5].iconoff;

        break;
      case "step4":
        if ($scope.step4) {
          $scope.step4img = $scope.stepsDesription[3].iconon;
        }
        else {
          $scope.step4img = $scope.stepsDesription[3].iconoff;
        }


        $scope.step5img = $scope.stepsDesription[4].iconoff;
        $scope.step6img = $scope.stepsDesription[5].iconoff;
        break;
      case "step5":
        if ($scope.step5) {
          $scope.step5img = $scope.stepsDesription[4].iconon;
        }
        else {
          $scope.step5img = $scope.stepsDesription[4].iconoff;
        }

        $scope.step6img = $scope.stepsDesription[5].iconoff;

        break;
      case "step6":
        if ($scope.step6) {
          $scope.step6img = $scope.stepsDesription[5].iconon;
        }
        else {
          $scope.step6img = $scope.stepsDesription[5].iconoff;
        }
        break;
      default:

    }


  };

  function hideCurrentStep(step) {

    switch ($scope.currentStep) {
      case "step1":
        $scope.step1 = false;
        break;
      case "step2":
        $scope.step2 = false;
        break;
      case "step3":
        $scope.step3 = false;
        break;
      case "step4":
        $scope.step4 = false;
        break;
      case "step5":
        $scope.step5 = false;
        break;
      case "step6":
        $scope.step6 = false;
        break;
      default:

    }
    $scope.currentStep = step;

  }

  function nextStep(step) {
    switch (step) {
      case "step1":
        $scope.step1 = true;
        break;
      case "step2":
        $scope.step2 = true;
        break;
      case "step3":
        $scope.step3 = true;
        break;
      case "step4":
        $scope.step4 = true;
        initFlexSliders();
        break;
      case "step5":
        $scope.step5 = true;
        break;
      case "step6":
        $scope.step6 = true;
        break;
      case "stepSuccess":
        $scope.stepSuccess = true;
        break;

      default:

    }
    //change progressbar image
    $scope.toggleImage(step);
  }
}

superCrateHeaderController.$inject = ["$scope", "Apinator", "config"];

function superCrateHeaderController($scope, Apinator, config) {

  $scope.total = 0;
  $scope.totalVisible = 0;
  $scope.currentUser = {};
  $scope.linkToAccount = config.accountUrl;

  Apinator.POST("auth/current/user/get")
    .then(user => {
      $scope.currentUser = user;
    })
    .catch(err => {
    });

  $scope.$on("cart-total", function(event, args) {

    $scope.total = args.total;
    // do what you want to do

  });

  $scope.srcollToForm = function() {

    var destination = $("#supercrateform").offset().top - 120;
    $("body").animate({
      scrollTop: destination
    }, 1100);
    $("html").animate({
      scrollTop: destination
    }, 1100);

  };

  $scope.summarytoggle = function() {

    $scope.mobile_summary_step = !$scope.mobile_summary_step;

  };


}

function extraServicesBox() {

  var directive = {
    template: require("./template/extra_services_box.html"),
    restrict: "E"
  };

  return directive;
}

function paymentinfoBox() {

  var directive = {
    template: require("./template/payment_info_block.html"),
    restrict: "E"
  };

  return directive;
}

function paymentBox() {

  var directive = {
    template: require("./template/payment_dialog.html"),
    restrict: "E"
  };

  return directive;
}


// SUPERCRATE HEADER
function superCrateHeader() {

  var directive = {
    controller: "superCrateHeaderController",
    template: require("./template/superCrateHeader.html"),
    restrict: "E"
  };

  return directive;
}


// SUPERCRATE FORM

function superCrateForm() {
  let directive = {
    controller: "superCrateFormController",
    template: require("./template/superCrateForm.html"),
    restrict: "E"
  };

  return directive;
}


// Zip Code Directive
processZipCode.$inject = ["$rootScope", "$http", "superCrateServices"];

function processZipCode($rootScope, $http, superCrateServices) {
  return {
    restrict: "A",
    require: "?ngModel",
    link: function(scope, element, attrs, ngModel) {
      element.bind("change", function() {
        if (element.val().length != 5) {
          element.addClass("error");
          element.val("");
        }
      });

      scope.$watch(attrs.ngModel, function(zip_code) {

        if (!angular.isUndefined(zip_code)) {
          element.removeClass("success");

          if (zip_code.length == 5) {
            element.removeClass("error");

            var geocoder = new google.maps.Geocoder();
            var type = attrs.ngModel;

            geocoder.geocode({
              "address": zip_code,
              "country": "US"
            }, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                var arrAddress = results[0]["address_components"];
                var address = superCrateServices.parseZipToAddress(arrAddress, type);

                //1. Get if it's in the pologin
                var inLocation = superCrateServices.ContainsLocation(results);


                if (type == "order.zip_dropoff") {

                  scope.order.addressFrom = address;
                  scope.order.extraDistanceDropoff = inLocation;
                  //Check if we delivery
                  //If not show validatio and message
                  //If yes and we charge show message with extra charge
                  scope.dropoff_msg = "We have extra fee for delivery $20";
                  element.addClass("success");
                }
                else {
                  scope.order.addressTo = address;
                  scope.order.extraDistancePickup = inLocation;
                  //If not show validatio and message
                  //If yes and we charge show message with extra charge
                  scope.pickup_msg = "We have extra fee for pickup $20";
                  element.addClass("success");
                }

                //INIT FIELDS
                if (!angular.isUndefined(address.state) && address.country == "US") {

                  // showAddress(address, type)
                }
                else {

                  if (type == "request.zipFrom") {
                    angular.element("#calc-info-steps .box_info .moving-from").empty().append(
                      "<h3>Moving From:</h3><span class=\"error_alarm\">We do not service zip " + address.postal_code + ".</span>").show(
                      "slow");
                  } else {
                    angular.element("#calc-info-steps .box_info .moving-to").empty().append(
                      "<h3>Moving To:</h3><span class=\"error_alarm\">We do not service zip " + address.postal_code + ".</span>").show(
                      "slow");
                  }

                  element.addClass("error");
                  element.val("");
                  element.removeClass("success");
                }
              } else {
                if (type == "request.zipFrom") {
                  angular.element("#calc-info-steps .box_info .moving-from").empty().append(
                    "<h3>Moving From:</h3><span class=\"error_alarm\">Not found zip code " + zip_code + ".</span>").show(
                    "slow");
                } else {
                  angular.element("#calc-info-steps .box_info .moving-to").empty().append(
                    "<h3>Moving To:</h3><span class=\"error_alarm\">Not found zip code " + zip_code + ".</span>").show(
                    "slow");
                }

                element.addClass("error");
                element.val("");
                element.removeClass("success");
              }
            });
          }
        }
      });
    }
  };
}

function validatePhone() {
  return {
    restrict: "A",
    require: "?ngModel",
    link: function(scope, element, attrs) {
      element.bind("change", function() {
        if (element.val().length < 14) {
          element.addClass("error");
          element.removeClass("success");
          element.removeClass("desktopzip");
          scope.validation_array.client_phone = false;
        }
      });

      scope.$watch(attrs.ngModel, function(nval) {
        if (!angular.isUndefined(nval)) {
          if (element.val().length == 14) {
            element.removeClass("error");
            element.addClass("success");
            element.addClass("desktopzip");
            scope.validation_array.client_phone = true;
          }
        }
      });
    }
  };
}

function sfIsEmail() {
  return {
    restrict: "A",
    link: function(scope, element) {
      element.bind("change", function() {
        if (element.val().length == 0) {
          angular.element("#edit-confirmemail").val("");
        }
        else {
          var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          var ismail = regex.test(element.val());

          if (!ismail) {
            element.removeClass("success");
            element.removeClass("desktopzip");
            element.addClass("error");
            element.val("");
            scope.validation_array.client_email = false;
          }
          else {
            element.removeClass("error");
            element.addClass("success");
            element.addClass("desktopzip");
            scope.validation_array.client_email = true;
          }

        }
      });
    }
  };
}

function ccCreditCard() {
  return {
    restrict: "A",
    scope: {},
    link: function(scope, element) {

      element.bind("change paste keyup", function() {

        let credit_number = element.val();

        let type = GetCardType(credit_number);

        if (credit_number.length >= 15) {

          if (type == "AMEX") {
            $("#cvc").attr("maxlength", 4);
            if (credit_number.length == 15) {
              element.removeClass("error");
              element.addClass("success");
              element.addClass("desktopzip");
            }

          } else {
            $("#cvc").attr("maxlength", 3);
            if (!type && (credit_number.length == 16)) {
              element.addClass("error");
              element.removeClass("success");
              element.removeClass("desktopzip");
            }
            else {
              element.removeClass("error");
              element.addClass("success");
              element.addClass("desktopzip");
            }
          }

        }
        else {
          element.removeClass("success");
          element.removeClass("desktopzip");
        }


      });

      function GetCardType(number) {
        // visa
        let re = new RegExp("^4");
        if (number.match(re) != null) {
          return "Visa";
        }

        // Mastercard
        re = new RegExp("^5[1-5]");
        if (number.match(re) != null) {
          return "Mastercard";
        }

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null) {
          return "AMEX";
        }

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null) {
          return "Discover";
        }

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null) {
          return "Visa Electron";
        }

        return "";
      }
    }

  };
}
