"use strict";

const Variables = {};

let angular = window.angular;
angular
  .module("superCrate")
  .factory("Variables", VariablesService);

VariablesService.$inject = ["Apinator", "config", "Timezone"];

function VariablesService(Apinator, config, Timezone) {
  let initialResolve;
  let service = {};
  service.getFrontVariables = getFrontVariables;
  service.Variables = Variables;
  service.initialPromise = new Promise(res => {
    initialResolve = res;
  });
  return service;

  function getFrontVariables() {
    return Promise.all([
      Apinator.GATE("getVariablesAnonimus", {})
        .then(res => {
          Variables.companyInfo = res.companyInfo;
          Variables.packagesInfo = res.packagesInfo;
          Variables.packagesInfo.supplies.forEach(supply => supply.image = supply.image);
          Variables.servicesInfo = res.servicesInfo;
        }),
      Apinator.GATE('getTimeZone', {})
        .then(res => {
          Timezone.setupTimezone(res);
        })
        //todo remove catch
        .catch(err => {
          console.error(err);
          throw new Error('Cannot get company timezone');
        })
    ])
      .then(res => {
        initialResolve();
      });
  }
}