'use strict';

let angular = window.angular;
angular
	.module('superCrate')
	.factory('Apinator', Apinator);

Apinator.$inject = ['$http', '$rootScope', '$q', 'config', '$location'];

let lastRecalculatingPromise;

function Apinator($http, $rootScope, $q, config, $location) {
	let service = {};
	service.GATE = GATE;
	service.GET = GET;
	service.POST = POST;
	service.calculateRequest = calculateRequest;
	return service;

	function getDefaultHeaders() {
		return {
			'withCredentials': true
		};
	}

	function getDefaultBody(taskName, taskBody) {
		return {
			businessData: taskBody,
			systemData: {
				taskName: taskName,
			},
		};
	}

	function GATE(taskName, data = {}) {
		let headers = getDefaultHeaders();
		headers.data = getDefaultBody(taskName, data);
		headers.method = 'POST';
		headers['content-type'] = 'application/json';
		headers.url = config.serverUrl + 'company';

		return new Promise((res, rej) => {
			$http(headers)
				.success(data => {
					res(data);
				})
				.error(function (error, status) {
					rej(error);
				});
		});
	}

	function GET(url, data) {

		return new Promise((res, rej) => {
			$http.get(config.serverUrl + url, {params: data})
				.success(data => {
					res(data);
				})
				.error(function (error, status) {
					rej(error);
				});
		});
	}

	function POST(url, data = {}) {

		let headers = getDefaultHeaders();
		headers.data = data;
		headers.method = 'POST';
		headers['content-type'] = 'application/json';
		headers.url = config.serverUrl + url;

		return new Promise((res, rej) => {
			$http(headers)
				.success(data => {
					res(data);
				})
				.error(function (error, status) {
					rej(error);
				});
		});
	}

	function calculateRequest(request) {
		let currentPromise = GATE('calculateRequest', request);
		lastRecalculatingPromise = currentPromise;
		currentPromise
			.then(res => {
				if (currentPromise !== lastRecalculatingPromise) return {isOldPromise: true};
				return res;
			});

		return currentPromise;
	}
}