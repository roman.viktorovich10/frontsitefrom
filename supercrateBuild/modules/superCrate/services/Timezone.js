"use strict";
import moment from 'moment';
import 'moment-timezone';
import timezoneData from './timezonesData';

const MOMENT_PARSING_FORMAT = 'YYYY-MM-DDTHH:mm:ss.sss';

let angular = window.angular;
angular
  .module("superCrate")
  .factory("Timezone", TimezoneService);

TimezoneService.$inject = [];

function TimezoneService() {
  let service = {};
  service.setupTimezone = setupTimezone;
  service.moveDateToMomentTimezone = moveDateToMomentTimezone;
  service.moveMomentToBrowserTimezoneDate = moveMomentToBrowserTimezoneDate;
  service.moment = moment;

  return service;

  function setupTimezone(timezone) {
    moment.tz.add(timezone.fullString);
    moment.tz.setDefault(timezone.name);
  }

  function moveDateToMomentTimezone(valueDirty) {
    let value = new Date(valueDirty.getTime() - valueDirty.getTimezoneOffset() * 60000);

    return moment(value.toISOString(), MOMENT_PARSING_FORMAT);
  }

  function moveMomentToBrowserTimezoneDate(momentInst) {
    return new Date(momentInst.format(MOMENT_PARSING_FORMAT));
  }
}