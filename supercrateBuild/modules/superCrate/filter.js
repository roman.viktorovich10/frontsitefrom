'use strict';

let angular = window.angular;
angular
	.module('superCrate')
	.filter('tel', function () {
		return function (tel) {
			if (!tel) {
				return '';
			}

			var value = tel.toString().trim().replace(/^\+/, '');

			if (value.match(/[^0-9]/)) {
				return tel;
			}

			var country, city, number;

			switch (value.length) {
			case 10: // +1PPP####### -> C (PPP) ###-####
				country = 1;
				city = value.slice(0, 3);
				number = value.slice(3);
				break;

			case 11: // +CPPP####### -> CCC (PP) ###-####
				country = value[0];
				city = value.slice(1, 4);
				number = value.slice(4);
				break;

			case 12: // +CCCPP####### -> CCC (PP) ###-####
				country = value.slice(0, 3);
				city = value.slice(3, 5);
				number = value.slice(5);
				break;

			default:
				return tel;
			}

			if (country == 1) {
				country = '';
			}

			number = number.slice(0, 3) + '-' + number.slice(3);

			return (country + ' (' + city + ') ' + number).trim();
		};
	})
	.filter('reverse', function () {
		return function (items) {
			return items.slice().reverse();
		};
	})

	.filter('usatime', function () {

		return function (time) {

			var amtime;
			if (time > 12) {
				amtime = time - 12;
				if (amtime == 12) return '12AM';
				return amtime + 'PM';
			}
			else {

				if (time == 12) return '12PM';
				return time + 'AM';

			}


		};


	})
	.filter('range', function () {
		return function (input, total) {
			total = parseInt(total);

			for (var i = 0; i < total; i++) {
				input.push(i);
			}

			return input;
		};
	})

	.filter('decToTime', function () {
		return function (input) {

			var hrs = parseInt(Number(input));
			var min = Math.round((Number(input) - hrs) * 60);
			if (hrs < 1) {
				return min + ' min';
			} else {
				return hrs + ' h : ' + min + ' min';
			}

		};
	})
	.filter('numToTime', function () {
		return function (input) {

			var zero = '0';
			var zero2 = '0';
			var hrs = parseInt(Number(input));
			var fraction = Number(input) - hrs;
			var min = 0;
			// 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75.
			if (fraction > 0.15 && fraction < 0.36) {
				min = 15;
			}
			if (fraction >= 0.36 && fraction < 0.64) {
				min = 30;
			}
			if (fraction >= 0.64 && fraction < 0.87) {
				min = 45;
			}
			if (fraction >= 0.87) {
				min = 0;
				hrs++;
			}

			if (hrs > 9) {
				zero = '';
			}
			if (min > 9) {
				zero2 = '';
			}
			if (min == 0) {
				return zero + hrs + ':00';
			}
			else if (hrs == 0) {
				return '00:'.min;
			}
			else {
				return zero + hrs + ':' + zero2 + min;
			}


		};
	})
	.filter('hrmin', function () {
		return function (number) {

			var zero = '0';
			var zero2 = '0';
			var minutes = 0;
			var hour = Math.floor(number);
			var fraction = number - hour;

			// 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75
			if (fraction > 0.15 && fraction < 0.36) {
				minutes = 15;
			}
			if (fraction >= 0.36 && fraction < 0.64) {
				minutes = 30;
			}
			if (fraction >= 0.64 && fraction < 0.87) {
				minutes = 45;
			}
			if (fraction >= 0.87) {
				minutes = 0;
				hour++;
			}


			if (minutes == 0 && hour != 1) {
				return hour + ' hrs';
			} else if (hour == 1 && minutes == 0) {
				return hour + ' hr';
			} else if (hour == 1 && minutes != 0) {
				return hour + ' hr ' + minutes + ' min';
			} else if (hour == 0) {
				return minutes + ' min';
			} else {
				return hour + ' hrs ' + minutes + ' min';
			}


		};
	})
	.filter('ago', function () {
		return function (input) {


			var date = moment.unix(input).utc();
			return moment(date).fromNow();


		};
	});
