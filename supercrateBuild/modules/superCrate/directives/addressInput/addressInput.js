'use strict';

import './addressInput.scss';

let angular = window.angular;
let $ = window.$;
angular
	.module('superCrate')
	.controller('addressInputCtrl', addressInputCtrl)
	.directive('addressInput', addressInput);

addressInput.$inject = [];

function addressInput() {
	return {
		controller: addressInputCtrl,
		template: require('./addressInput.html'),
		restrict: 'A',
		scope: {
			placeholder: '=',
			value: '=',
			sendMessage: '&',
      sendError: '&',
		},
	};

}

addressInputCtrl.$inject = ['$scope', 'Apinator', '$sce'];

function addressInputCtrl($scope, Apinator, $sce) {
	$scope.isInvalid = false;
	$scope.isValid = false;
	$scope.innerValue = fullAddressRow($scope.value);
	$scope.autocompleteRows = [];
	let autocompleteSources = [];
	$scope.onChangeInnerValue = onChangeInnerValue;
	$scope.selectRow = selectRow;

	function fullAddressRow(value) {
		if (!value) return '';
		let result = '';
		if (value.address) result += value.address;
		if (value.city) result += ', ' + value.city;
		if (value.state) result += ', ' + value.state;
		if (value.postal) result += ', ' + value.postal;
		return result;
	}

	function selectRow(index) {
		let place = autocompleteSources[index];
		$scope.innerValue = place.description;
		$scope.autocompleteRows = [];
		let fieldsForUpdate = {};
		let components = {
			country: ['us'],
		};

		let addressForGeocoding;
		fieldsForUpdate.address = true;
		fieldsForUpdate.city = true;
		fieldsForUpdate.state = true;
		fieldsForUpdate.postal = true;
		addressForGeocoding = place.description;

		Apinator.GATE('geocodeAddress', {
			address: addressForGeocoding,
			components
		})
			.then(res => fillAddressInputs(fieldsForUpdate, res[0]));
	}

	function fillAddressInputs(fieldsForUpdate, {
		address_components: addressComponents,
		formatted_address: formattedAddress,
		types,
	}) {

		let address = {};


		if (fieldsForUpdate.streetNumber || fieldsForUpdate.address) {
			address.streetNumber = '';
			let streetNumber = _.find(addressComponents,
				component => component.types.indexOf('street_number') !== -1);
			if (streetNumber) {
				address.streetNumber = streetNumber.short_name;
			}
		}

		if (fieldsForUpdate.address) {
			if (types.indexOf('street_address') !== -1
				|| types.indexOf('route') !== -1
				|| types.indexOf('intersection') !== -1
				|| types.indexOf('parking') !== -1
				|| types.indexOf('bus_station') !== -1
				|| types.indexOf('train_station') !== -1
				|| types.indexOf('transit_station') !== -1
				|| types.indexOf('premise') !== -1
				|| types.indexOf('subpremise') !== -1
				|| types.indexOf('airport') !== -1
				|| types.indexOf('point_of_interest') !== -1) {
				address.address = formattedAddress.split(', ')[0];
			} else {
				address.address = '';
			}
		}

		if (fieldsForUpdate.state) {
			let state = _.find(addressComponents,
				component => component.types.indexOf('administrative_area_level_1') !== -1);
			address.state = state.short_name || '';
		}

		if (fieldsForUpdate.city) {
			let indexInAddress = 0;
			if (types.indexOf('street_address') !== -1
				|| types.indexOf('route') !== -1
				|| types.indexOf('intersection') !== -1
				|| types.indexOf('parking') !== -1
				|| types.indexOf('bus_station') !== -1
				|| types.indexOf('train_station') !== -1
				|| types.indexOf('transit_station') !== -1
				|| types.indexOf('premise') !== -1
				|| types.indexOf('subpremise') !== -1
				|| types.indexOf('airport') !== -1
				|| types.indexOf('point_of_interest') !== -1) {
				indexInAddress = 1;
			}
			address.city = formattedAddress.split(', ')[indexInAddress];
		}

		if (fieldsForUpdate.postal) {
			let postal = _.find(addressComponents,
				component => component.types.indexOf('postal_code') !== -1);
			address.postal = postal ? postal.short_name : '';
		}

		validateAddress(address)
			.then(res => {
				$scope.isInvalid = false;
				$scope.isValid = true;
				$scope.value.streetNumber = address.streetNumber;
				$scope.value.address = address.address;
				$scope.value.state = address.state;
				$scope.value.city = address.city;
				$scope.value.postal = address.postal;
				if (res.cost) {
					$scope.sendMessage({msg: `We have extra delivery charge for this address $${res.cost}`});
				} else {
					$scope.sendMessage({msg: null});
				}
			})
			.catch(err => {
				$scope.isInvalid = true;
				$scope.isValid = false;
				$scope.value.streetNumber = '';
				$scope.value.address = '';
				$scope.value.state = '';
				$scope.value.city = '';
				$scope.value.postal = '';
				$scope.sendError({msg: err});
				console.error(err);
			})
			.finally(() => {
				$scope.innerValue = fullAddressRow($scope.value);
				$scope.$apply();
			});
	}

	function validateAddress(address) {
		return Apinator.GATE('getAreaForAddress', {address})
			.then(res => {
				if (res === null) {
					throw 'NOT_VALID';
				}
				return res;
			})
			.catch(err => {
				if (err === 'NOT_VALID') {
					throw 'We don\'t move to this location';
				}
				throw 'Can not check address validity';
			});
	}

	function onChangeInnerValue() {
		if (!$scope.innerValue.length) {
			$scope.autocompleteRows = [];
		} else {
			Apinator.GATE('autocompleteAddress', {
				address: $scope.innerValue,
				types: ['address'],
				components: {
					country: ['us'],
				},
			})
				.then(parseRows)
				.catch(err => console.err);
		}
	}

	function parseRows(rows) {
		autocompleteSources = rows;
		$scope.autocompleteRows = autocompleteSources.map(place => {
			let resultSpans = [];
			let lastIndex = 0;
			place.structured_formatting.main_text_matched_substrings.forEach(substringInfo => {
				resultSpans.push(place.structured_formatting.main_text.slice(lastIndex, substringInfo.offset));
				resultSpans.push(
					`<span class='AddressInput__row__highlighted'>
	${place.structured_formatting.main_text.slice(substringInfo.offset,
						substringInfo.offset + substringInfo.length)}
</span>`);
				lastIndex = substringInfo.offset + substringInfo.length;
			});
			resultSpans.push(place.structured_formatting.main_text.slice(lastIndex));
			resultSpans.push(
				`<span class='AddressInput__row__secondary'>
	${place.structured_formatting.secondary_text}
</span>`);

			return $sce.trustAsHtml(resultSpans.join(''));
		});

		$scope.$apply();
	}
}