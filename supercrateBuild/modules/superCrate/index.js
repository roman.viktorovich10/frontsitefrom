'use strict';

let angular = window.angular;

angular.module('superCrate',
	[
		'ngMask',
		'ngAnimate',
		'angular-datepicker',
	]);

require('./config');
require('./services');

require('./superCrateApp');
require('./superCrateServices');
require('./directives');
require('./filter');

require('./directives/addressInput/addressInput');

require('./styles/supercrate.css');
require('./styles/progressbar.css');
require('./styles/mobile.css');