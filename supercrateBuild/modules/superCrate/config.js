'use strict';
let angular = window.angular;

angular
	.module('superCrate')
	.constant('config', {
		appName: 'superCrate',
		appVersion: 1.0,
		host: '/',
		accountUrl: '/account',
		serverUrl: '/api/',
		assets: '/wp-content/themes/shopscape/'
	});
