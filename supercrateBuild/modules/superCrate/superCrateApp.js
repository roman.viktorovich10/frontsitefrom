import moment from 'moment';
import 'moment-timezone';

'use strict';
let angular = window.angular;

angular
	.module('superCrate')
	.controller('superCrateController', superCrateController);

superCrateController.$inject = ['$scope', '$rootScope', 'Apinator', '$timeout', 'Variables', 'Timezone'];
function superCrateController($scope, $rootScope, Apinator, $timeout, Variables, Timezone) {

	Variables.getFrontVariables()
		.then(() => {
			$timeout(function () {
				$scope.loading = false;
			}, 500);

			$scope.MoveDatePickerOptions = {
				min: '',
				disable: [],
				calendar: {},
				calendarTypes: {}

			};
			$scope.StorageDatePickerOptions = {
				min: '',
				disable: [],
				calendar: {},
				calendarTypes: {}
			};

			let CurrentDay = moment().startOf('day');

			$scope.MoveDatePickerOptions.min = Timezone.moveMomentToBrowserTimezoneDate(CurrentDay);
			$scope.MoveDatePickerOptions.calendar = {};//$scope.calendar;
			$scope.MoveDatePickerOptions.calendarTypes = {};//$scope.basicsettings.ratesSettings;

			let StorageDay = moment(CurrentDay).add(2, 'days');
			$scope.StorageDatePickerOptions.min = Timezone.moveMomentToBrowserTimezoneDate(StorageDay);
			$scope.StorageDatePickerOptions.calendar = {};//$scope.calendar;
			$scope.StorageDatePickerOptions.calendarTypes = {};//$scope.basicsettings.ratesSettings;

			// END OF SERVCIES
		});
}
