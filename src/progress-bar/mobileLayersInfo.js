import icon1 from './images/iconMobile1.png';
import icon2 from './images/iconMobile2.png';
import icon3 from './images/iconMobile3.png';
import icon4 from './images/iconMobile4.png';
import icon5 from './images/iconMobile5.png';
const tree1 = require('./images/mobileTree1.png');
const tree2 = require('./images/mobileTree2.png');
const tree3 = require('./images/mobileTree3.png');
const tree4 = require('./images/mobileTree4.png');
const tree5 = require('./images/mobileTree5.png');
const tree6 = require('./images/mobileTree6.png');
const tree7 = require('./images/mobileTree7.png');
const tree8 = require('./images/mobileTree8.png');

const layersInfo = [
	{
		backgroundStyle: {
			backgroundImage: `url(${tree1})`,
		},
		movableStyle: {
			transform: 'translate(-3%, -58%)',
			zIndex: 9
		},
		className: 'item',
		step: 0,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree2})`,
		},
		movableStyle: {
			transform: 'translate(-2%, -53%)',
			zIndex: 8
		},
		className: 'item',
		step: 0,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree3})`,
		},
		movableStyle: {
			transform: 'translate(21%, -56%)',
			zIndex: 9
		},
		className: 'item',
		step: 1,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree7})`,
		},
		movableStyle: {
			transform: 'translate(72%, -54%)',
			zIndex: 8
		},
		className: 'item',
		step: 4,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree6})`,
		},
		movableStyle: {
			transform: 'translate(75%, -50%)',
			zIndex: 8
		},
		className: 'item',
		step: 4,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree5})`,
		},
		movableStyle: {
			transform: 'translate(62%, -62%)',
			zIndex: 8
		},
		className: 'item',
		step: 3,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree4})`,
		},
		movableStyle: {
			transform: 'translate(32%, -57%)',
			zIndex: 8
		},
		className: 'item',
		step: 2,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${tree8})`,
		},
		movableStyle: {
			transform: 'translate(69%, -58%)',
			zIndex: 9
		},
		className: 'item',
		step: 4,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon1})`,
		},
		movableStyle: {
			transform: 'translate(7%, -34%)',
			zIndex: 16
		},
		className: 'icon',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon4})`,
		},
		movableStyle: {
			transform: 'translate(31%, -34%)',
			zIndex: 16
		},
		className: 'icon',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon2})`,
		},
		movableStyle: {
			transform: 'translate(50%, -34%)',
			zIndex: 16
		},
		className: 'icon',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon3})`,
		},
		movableStyle: {
			transform: 'translate(70%, -34%)',
			zIndex: 16
		},
		className: 'icon',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon5})`,
		},
		movableStyle: {
			transform: 'translate(90%, -34%)',
			zIndex: 16
		},
		className: 'icon',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(4%, 64%)',
			zIndex: 16
		},
		text: 'CHOOSE\nPACKAGE',
		className: 'text',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(26%, 64%)',
			zIndex: 16
		},
		text: 'EXTRA\nSUPPLIES',
		className: 'text',
		step: 1,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(45%, 64%)',
			zIndex: 16
		},
		text: 'PICK UP\nLOCATION',
		className: 'text',
		step: 2,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(67%, 64%)',
			zIndex: 16
		},
		text: 'SCHEDULE\nYOUR BINS',
		className: 'text',
		step: 3,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(86%, 64%)',
			zIndex: 16
		},
		text: 'PAY &\n CONFIRM',
		className: 'text',
		step: 4,
		order: 3
	},
];

export default layersInfo;