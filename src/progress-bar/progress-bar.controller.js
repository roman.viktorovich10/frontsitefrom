import Snap from 'snapsvg';

window.angular
	.module('superCrate')
	.controller('progressBarCtrl', progressBarCtrl);

const carImage = require('./images/car.png');

import mapObjects from './layersInfo';
import mobileMapObjects from './mobileLayersInfo';
import {
	getCarPath,
	getCarPathMobile,
	getBackgroundShapes,
	getMobileBackgroundShapes
} from './pathInfo';

const background = require('./images/background.png');
const backgroundMobile = require('./images/backgroundMobile.png');
const sizeMobile = require('./images/sizeMobile.png');
const sizeDesktop = require('./images/sizeDesktop.png');

const CAR_WIDTH = 5;
const CAR_HEIGHT = 23;
const CAR_WIDTH_MOBILE = 8.9;
const CAR_HEIGHT_MOBILE = 18;
const CAR_DURATION = 5000;
const DRAWABLE_CLASS = 'progress-bar__drawable';
const EXTERNAL_STEP_NAMES = ['step1', 'step2', 'step3', 'step4', 'step6', 'step6'];
const CAR_POSITIONS = [.22, .38, .56, .75, .97, .97];
const CAR_POSITIONS_MOBILE = [.13, .33, .53, .73, .92, .92];

progressBarCtrl.$inject = ['$scope', '$element'];

function progressBarCtrl($scope, $element) {
	const paperCar = Snap('#progress-bar_car_svg');
	const paper = Snap('#progress-bar_background_svg');

	const shapeGroup = paper.el('g', {width: '100%'});
	let theCarG = {};
	let theCarG2 = {};
	let theCar = {};
	let theCarImage = {};
	let theBackground = {};
	let actualBackground;
	let shapes;
	let theShape = {};
	let pathSvg = {};
	let scaleDetector = {};
	let mask = {};
	let thePath = {};
	$scope.step = 0;
	$scope.carStyle = {
		backgroundImage: `url(${carImage})`
	};
	let carPosition = 0;
	let totalLenght;
	initStyles();
	initImages();
	replaceImages();
	onChangeExternalStep();
	calculateObjectsStyles();
	$scope.$watch('step', onChangeStep);
	$scope.$watch('step2', onChangeExternalStep);
	$scope.$watch('step3', onChangeExternalStep);
	$scope.$watch('step4', onChangeExternalStep);
	$scope.$watch('step5', onChangeExternalStep);
	$scope.$watch('step6', onChangeExternalStep);
	$scope.clickStep = clickStep;

	$element.find('img')[0].addEventListener('load', () => {
		initStyles();
		initImages();
		replaceImages();
		calculateObjectsStyles();
		alignCar();
		moveTheCar();
		$scope.$apply();
	});
	window.addEventListener('resize', () => {
		initStyles();
		initImages();
		replaceImages();
		calculateObjectsStyles();
		alignCar();
		$scope.$apply();
	});

	function onChangeExternalStep() {
		$scope.step = getStepByExternals();
	}

	function getStepByExternals() {
		if ($scope.step2) return 1;
		if ($scope.step3) return 2;
		if ($scope.step4) return 3;
		if ($scope.step5) return 4;
		if ($scope.step6) return 5;
		return 0;
	}

	function clickStep(index) {
		//$scope.step = index;
		$scope.clickStepExternal({stepName: EXTERNAL_STEP_NAMES[index]});
	}

	function onChangeStep(newValue, oldValue) {
		if (newValue == oldValue) return;
		calculateObjectsStyles();
		calculateBackgroundStyle(newValue > oldValue);
		moveTheCar();
	}

	function alignCar() {
		let isMobile = window.innerWidth < 1000;
		totalLenght = Snap.path.getTotalLength(thePath);
		let moveToPoint = Snap.path.getPointAtLength(thePath,
			totalLenght * (isMobile ? CAR_POSITIONS_MOBILE : CAR_POSITIONS)[$scope.step]);
		let x = moveToPoint.x;
		let y = moveToPoint.y;
		theCarG.transform(
			`translate(${x}, ${y}) rotate(${moveToPoint.alpha})`);
	}

	function moveTheCar() {
		let isMobile = window.innerWidth < 1000;
		totalLenght = Snap.path.getTotalLength(thePath);
		Snap.animate(carPosition, totalLenght * (isMobile ? CAR_POSITIONS_MOBILE : CAR_POSITIONS)[$scope.step], val => {
			carPosition = val;
			let moveToPoint = Snap.path.getPointAtLength(thePath, carPosition);
			let x = moveToPoint.x;
			let y = moveToPoint.y;
			theCarG.transform(
				`translate(${x}, ${y}) rotate(${moveToPoint.alpha})`);
		}, CAR_DURATION, mina.linear);
	}

	function calculateObjectsStyles() {
		let len = $scope.mapObjects.length;
		for (let index = 0; index < len; index++) {
			let className = DRAWABLE_CLASS + ' ' + DRAWABLE_CLASS + '__' + $scope.mapObjects[index].className;
			if ($scope.mapObjects[index].step <= +$scope.step) {
				className += ` ${DRAWABLE_CLASS}__${$scope.mapObjects[index].className}__visible`;
				className += ` ${DRAWABLE_CLASS}__${$scope.mapObjects[index].className}__visible__${$scope.mapObjects[index].order}`;
			}
			$scope.mapObjects[index].backgroundClass = className;
		}
	}

	function getMinaWithDelay(duration, delay, minaFunc) {
		if (delay == 0) return minaFunc;
		let relativeDelay = delay / (delay + duration);
		let relativeDelayInverse = 1 - relativeDelay;
		return (n) => {
			return n > relativeDelay ? minaFunc((n - relativeDelay) / relativeDelayInverse) : 0;
		};
	}

	function calculateBackgroundStyle(direction) {
		let duration = 500;
		let delay = direction ? 0 : 1000;
		const timingFunction = getMinaWithDelay(duration, delay, mina.linear);
		theShape.animate({d: shapes[$scope.step]}, duration + delay, timingFunction);
	}

	function initStyles() {
		let isMobile = window.innerWidth < 1000;
		$scope.isMobile = isMobile;
		$scope.sizeImage = isMobile ? sizeMobile : sizeDesktop;
		$scope.mainClass = isMobile
								 ? ['progress-bar', 'progress-bar_mobile']
								 : ['progress-bar', 'progress-bar_desktop'];
		$scope.mainStyle = isMobile
								 ? {transform: `translateX(-30px) translateY(-55px) scale(${window.innerWidth / 540})`}
								 : {};
	}

	function initImages() {
		let isMobile = window.innerWidth < 1000;
		actualBackground = isMobile ? backgroundMobile : background;
		$scope.mapObjects = isMobile ? mobileMapObjects : mapObjects;
		shapes = isMobile ? getMobileBackgroundShapes(1, 1, 0, 0) : getBackgroundShapes(1, 1, 0, 21.3);
	}

	function replaceImages() {
		if (theBackground.remove) theBackground.remove();
		if (theShape.remove) theShape.remove();
		if (mask.remove) mask.remove();
		if (thePath.remove) thePath.remove();
		if (theCarImage.remove) theCarImage.remove();
		if (theCar.remove) theCar.remove();
		if (theCarG2.remove) theCarG.remove();
		if (theCarG.remove) theCarG.remove();
		if (pathSvg.remove) pathSvg.remove();
		if (scaleDetector.remove) scaleDetector.remove();

		let isMobile = window.innerWidth < 1000;

		pathSvg = isMobile
					 ? paper.svg(0, 0, '100%', '100%', 0, -61, 540, 173)
					 : paper.svg(0, 0, '100%', '100%', 0, 0, 930, 132);

		theShape = pathSvg.path(shapes[$scope.step]).attr(
			{
				fill: 'white',
			}
		);

		theBackground = paper.image(actualBackground, 0, 0, '100%', '100%');
		if (!isMobile) {
			theBackground
				.attr({
					mask: 'url(#progress-bar-mask)',
				}).transform('translateX(13%)');
		}
		shapeGroup
			.append(theBackground);
		mask = paper.mask({id: 'progress-bar-mask'}).toDefs().append(pathSvg);

		scaleDetector = isMobile
							 ? paper.svg(0, 0, '100%', '100%', 0, 0, 540, 173)
							 : paper.svg(0, 0, '100%', '100%', 0, 0, 930, 132);
		let scale = scaleDetector.transform().globalMatrix.a;

		thePath = isMobile
					 ? paper.path(getCarPathMobile(scale, scale, 0, 80)).attr(
				{
					fill: 'none',
					stroke: 'none'
				}
			)
					 : paper.path(getCarPath(scale, scale, 4, 53)).attr(
				{
					fill: 'none',
					stroke: 'none'
				}
			);

		let width, height;
		if (navigator.userAgent.indexOf('Firssefox') !== -1) {
			width = -CAR_WIDTH / 2;
			height = isMobile ? -CAR_HEIGHT * 0.75 : -CAR_HEIGHT;
		} else {
			width = isMobile ? CAR_WIDTH_MOBILE / 2 : CAR_WIDTH / 2;
			height = isMobile ? CAR_HEIGHT_MOBILE : CAR_HEIGHT;
		}

		theCarG = paperCar.el('g', {
			'class': 'progress-bar__car',
		});

		theCar = theCarG.svg(-width + '%', 0,
			(isMobile ? CAR_WIDTH_MOBILE : CAR_WIDTH) + '%',
			(isMobile ? CAR_HEIGHT_MOBILE : CAR_HEIGHT) + '%', 0, 0, 48, 31);

		theCarImage = isMobile
						  ? theCar.image(carImage).transform(`rotate(180deg) translate(-48 -31)`)
						  : theCar.image(carImage).transform(`rotate(180deg) translate(-48 -31)`);

	}
}