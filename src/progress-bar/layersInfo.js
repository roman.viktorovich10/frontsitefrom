const trees0Image = require('./images/trees0.png');
const trees1Image = require('./images/trees1.png');
const trees2Image = require('./images/trees2.png');
const trees4Image = require('./images/trees4.png');
const trees5Image = require('./images/trees5.png');
const far1Image = require('./images/far1.png');
const far2Image = require('./images/far2.png');
const far2_2Image = require('./images/far2_2.png');
const far3Image = require('./images/far3.png');
const far4Image = require('./images/far4.png');
const rays1Image = require('./images/rays1.png');
const rays2Image = require('./images/rays2.png');
const rays3Image = require('./images/rays3.png');
const back1Image = require('./images/back1.png');
const back2Image = require('./images/back2.png');
const back3Image = require('./images/back3.png');
const back4Image = require('./images/back4.png');
const icon1Image = require('./images/locationoff.png');
const icon2Image = require('./images/scheduleoff.png');
const icon3Image = require('./images/extrasuppliesoff.png');
const icon4Image = require('./images/completeoff.png');

const layersInfo = [

	{
		backgroundStyle: {
			backgroundImage: `url(${rays3Image})`,
		},
		movableStyle: {
			transform: 'translate(85%, -33%)',
			zIndex: 13,
			opacity: '0.4',
		},
		className: 'rays',
		step: 4,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${rays1Image})`,
		},
		movableStyle: {
			transform: 'translate(19%, 0%)',
			zIndex: 13,
			opacity: '0.3',
		},
		className: 'rays',
		step: 2,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${rays2Image})`,
		},
		movableStyle: {
			transform: 'translate(27%, 0%)',
			zIndex: 13,
			opacity: '0.5',
		},
		className: 'rays',
		step: 2,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${rays2Image})`,
		},
		movableStyle: {
			transform: 'translate(10%, 0%)',
			zIndex: 13,
			opacity: '0.5',
		},
		className: 'rays',
		step: 1,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${rays1Image})`,
		},
		movableStyle: {
			transform: 'translate(1.5%, 0%)',
			zIndex: 13
		},
		className: 'rays',
		step: 1,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${back4Image})`,
		},
		backgroundButtonStyle: {
			width: '17.3%',
			left: '4%',
		},
		movableStyle: {
			transform: 'translate(74.2%, 0%)',
			zIndex: 7
		},
		className: 'background',
		step: 4,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${back3Image})`,
		},
		backgroundButtonStyle: {
			width: '18%',
			left: '5%',
		},
		movableStyle: {
			transform: 'translate(53.4%, 0%)',
			zIndex: 7
		},
		className: 'background',
		step: 3,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${back2Image})`,
		},
		backgroundButtonStyle: {
			width: '21%',
		},
		movableStyle: {
			transform: 'translate(36.7%, 0%)',
			zIndex: 7
		},
		className: 'background',
		step: 2,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${back1Image})`,
		},
		backgroundButtonStyle: {
			width: '17.75%',
		},
		movableStyle: {
			transform: 'translate(19.9%, 0%)',
			zIndex: 7
		},
		className: 'background',
		step: 1,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${trees0Image})`,
		},
		movableStyle: {
			transform: 'translate(0%, 0%)',
			zIndex: 14
		},
		className: 'item',
		step: 0,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${trees1Image})`,
		},
		movableStyle: {
			transform: 'translate(24.15%, -19.2%)',
			zIndex: 12
		},
		className: 'item',
		step: 1,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${trees2Image})`,
		},
		movableStyle: {
			transform: 'translate(38.53%, 0%)',
			zIndex: 14
		},
		className: 'item',
		step: 2,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${trees4Image})`,
		},
		movableStyle: {
			transform: 'translate(54%, -18%)',
			zIndex: 9
		},
		className: 'item',
		step: 3,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${trees5Image})`,
		},
		movableStyle: {
			transform: 'translate(84.3%, 0%)',
			zIndex: 14
		},
		className: 'item',
		step: 4,
		order: 1
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${far1Image})`,
		},
		movableStyle: {
			transform: 'translate(22.0%, -34.1%)',
			zIndex: 9
		},
		className: 'item',
		step: 1,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${far2Image})`,
		},
		movableStyle: {
			transform: 'translate(39.53%, -42%)',
			zIndex: 8
		},
		className: 'item',
		step: 2,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${far2_2Image})`,
		},
		movableStyle: {
			transform: 'translate(49.43%, -45%)',
			zIndex: 8
		},
		className: 'item',
		step: 2,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${far3Image})`,
		},
		movableStyle: {
			transform: 'translate(58.26%, -23.8%)',
			zIndex: 8
		},
		className: 'item',
		step: 3,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${far4Image})`,
		},
		movableStyle: {
			transform: 'translate(72.17%, -39%)',
			zIndex: 8
		},
		className: 'item',
		step: 4,
		order: 2
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(11.27%, 37%)',
			zIndex: 16
		},
		text: 'CHOOSE\nYOUR\nPACKAGE',
		className: 'text',
		step: 0,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon3Image})`,
		},
		movableStyle: {
			transform: 'translate(23%, -16%)',
			zIndex: 7
		},
		className: 'icon',
		step: 1,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(31%, 37%)',
			zIndex: 16
		},
		text: 'EXTRA\nSUPPLIES',
		className: 'text',
		step: 1,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon1Image})`,
		},
		movableStyle: {
			transform: 'translate(43%, -14%)',
			zIndex: 7
		},
		className: 'icon',
		step: 2,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(49.0%, 36%)',
			zIndex: 16
		},
		text: 'DROP OFF\nPICKUP\nLOCATION',
		className: 'text',
		step: 2,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon2Image})`,
		},
		movableStyle: {
			transform: 'translate(60%, -14%)',
			zIndex: 7
		},
		className: 'icon',
		step: 3,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(68.5%, 30%)',
			zIndex: 16
		},
		text: 'SCHEDULE\nYOUR\nBINS',
		className: 'text',
		step: 3,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: `url(${icon4Image})`,
		},
		movableStyle: {
			transform: 'translate(77%, -12%)',
			zIndex: 7
		},
		className: 'icon',
		step: 4,
		order: 3
	},
	{
		backgroundStyle: {
			backgroundImage: ``,
		},
		movableStyle: {
			transform: 'translate(82%, 42%)',
			zIndex: 16
		},
		text: 'PAYMENTS &\n CONFIRMATIONS',
		className: 'text',
		step: 4,
		order: 3
	},
];

export default layersInfo;