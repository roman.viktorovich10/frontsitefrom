'use strict';
import './progress-bar.styl';

window.angular.module('superCrate')
	.directive('progressBar', [progressBar]);

function progressBar() {
	return {
		restrict: 'E',
		template: require('./progress-bar.html'),
		controller: 'progressBarCtrl',
		scope: {
			step2: '=',
			step3: '=',
			step4: '=',
			step5: '=',
			step6: '=',
			clickStepExternal: '&',
		}
	};
}