const carPath = 'M 2.50,55.00\n' +
	'           C 2.50,55.00 63.04,73.98 128.25,79.00\n' +
	'             151.69,80.81 202.64,69.92 229.55,70.91\n' +
	'             314.77,75.31 325.50,71.00 346.50,63.00\n' +
	'             378.53,46.57 403.50,-2.00 465.50,2.00\n' +
	'             524.50,2.00 547.00,57.00 610.33,71.33\n' +
	'             627.00,74.00 826.78,75.64 863.50,65.00';

const carPathMobile = 'M -0.50,799.00\n' +
	'           C -0.24,798.92 0.03,798.84 0.29,798.77\n' +
	'             109.00,766.67 147.00,824.33 273.50,800.00\n' +
	'             390.33,777.33 468.50,802.50 539.67,797.33';

const backgroundShapes = [
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 195.00,478.02 410.00,478.02\n' +
	'             406.36,488.02 375.00,557.00 355.91,584.87\n' +
	'             358.52,585.13 363.16,585.64 376.22,586.98\n' +
	'             374.16,586.97 346.45,586.91 323.27,586.95\n' +
	'             319.87,586.95 184.88,586.97 184.88,586.97 Z',
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 349.00,478.00 564.00,478.00\n' +
	'             560.36,488.00 550.25,502.25 550.75,522.00\n' +
	'             552.25,550.25 536.00,541.50 512.91,586.99\n' +
	'             510.84,586.98 496.75,586.94 473.57,586.98\n' +
	'             470.17,586.98 184.88,586.97 184.88,586.97 Z',
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 457.00,478.00 672.00,478.00\n' +
	'             668.36,488.00 709.00,520.25 721.50,541.50\n' +
	'             748.25,583.00 782.50,573.25 773.57,586.99\n' +
	'             771.50,586.98 496.75,586.94 473.57,586.98\n' +
	'             470.17,586.98 184.88,586.97 184.88,586.97 Z',
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 685.13,478.03 900.12,478.03\n' +
	'             896.49,488.03 905.45,518.55 900.91,535.82\n' +
	'             892.91,566.00 888.18,572.36 880.81,586.99\n' +
	'             878.75,586.98 496.75,586.94 473.57,586.98\n' +
	'             470.17,586.98 184.88,586.97 184.88,586.97 Z',
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 839.58,478.01 1054.58,478.01\n' +
	'             1055.17,489.57 1060.00,527.67 1062.00,545.33\n' +
	'             1062.67,559.33 1060.25,569.00 1058.21,586.99\n' +
	'             1056.15,586.98 496.75,586.94 473.57,586.98\n' +
	'             470.17,586.98 184.88,586.97 184.88,586.97 Z',
	'M 184.88,586.97\n' +
	'           C 184.88,586.97 184.38,478.03 184.38,478.03\n' +
	'             184.38,478.03 839.58,478.01 1054.58,478.01\n' +
	'             1055.17,489.57 1060.00,527.67 1062.00,545.33\n' +
	'             1062.67,559.33 1060.25,569.00 1058.21,586.99\n' +
	'             1056.15,586.98 496.75,586.94 473.57,586.98\n' +
	'             470.17,586.98 184.88,586.97 184.88,586.97 Z'
];

const mobileBackgroundShapes = [
	'M 0.00,898.00\n' +
	'           C 0.00,898.00 0.00,784.25 0.00,784.25\n' +
	'             0.00,784.25 124.00,784.25 125.25,784.25\n' +
	'             102.33,837.67 106.18,898.00 106.18,898.00\n' +
	'             106.18,898.00 0.00,898.00 0.00,898.00 Z',
	'M 0.00,898.00\n' +
	'           C 0.00,898.00 0.00,784.25 0.00,784.25\n' +
	'             0.00,784.25 220.50,784.00 221.75,784.00\n' +
	'             193.00,841.67 238.67,845.33 216.38,897.97\n' +
	'             213.56,897.96 0.00,898.00 0.00,898.00 Z',
	'M 0.00,898.00\n' +
	'           C 0.00,898.00 0.00,784.25 0.00,784.25\n' +
	'             0.00,784.25 353.00,782.50 354.25,782.50\n' +
	'             321.00,816.67 325.00,872.00 325.00,897.96\n' +
	'             322.18,897.95 0.00,898.00 0.00,898.00 Z',
	'M 0.00,898.00\n' +
	'           C 0.00,898.00 0.00,784.25 0.00,784.25\n' +
	'             0.00,784.25 432.75,782.33 434.00,782.33\n' +
	'             410.00,830.55 454.00,834.55 434.00,898.00\n' +
	'             431.18,898.00 0.00,898.00 0.00,898.00 Z',
	'M 0.00,898.00\n' +
	'           C 0.00,898.00 0.00,784.25 0.00,784.25\n' +
	'             0.00,784.25 538.75,781.56 540.00,781.57\n' +
	'             540.00,783.40 540.00,896.05 539.99,898.00\n' +
	'             537.18,898.00 0.00,898.00 0.00,898.00 Z',
];

function getBackgroundShapes(scaleX, scaleY, offsetX, offsetY) {
	return backgroundShapes.map(shape => normalize(shape, scaleX, scaleY, offsetX, offsetY));
}

function getMobileBackgroundShapes(scaleX, scaleY, offsetX, offsetY) {
	let normalizedLast = normalize(_.last(mobileBackgroundShapes), scaleX, scaleY, offsetX, offsetY);
	return [normalizedLast,normalizedLast,normalizedLast,normalizedLast,normalizedLast,normalizedLast];
	//return mobileBackgroundShapes.map(shape => normalize(shape, scaleX, scaleY, offsetX, offsetY));
}

function getCarPath(scaleX, scaleY, offsetX, offsetY) {
	return normalize(carPath, scaleX, scaleY, offsetX, offsetY);
}

function getCarPathMobile(scaleX, scaleY, offsetX, offsetY) {
	return normalize(carPathMobile, scaleX, scaleY, offsetX, offsetY);
}

function normalize(path, scaleX, scaleY, offsetX, offsetY) {
	let dotsX = [];
	let dotsY = [];
	path.replace(/(-?\d+\.\d\d\,-?\d+\.\d\d)/g, dot => {
		let parsed = dot.split(',');
		dotsX.push(+parsed[0]);
		dotsY.push(+parsed[1]);
		return dot;
	});
	let maxX = Math.max.apply(null, dotsX);
	let minX = Math.min.apply(null, dotsX);
	let maxY = Math.max.apply(null, dotsY);
	let minY = Math.min.apply(null, dotsY);
	let defaultOffsetX = minX;
	let defaultOffsetY = minY;
	let width = maxX - minX;
	let height = maxY - minY;
	for (let i = 0; i < dotsX.length; i++) {
		dotsX[i] = (dotsX[i] - defaultOffsetX + (offsetX / 100) * width) * scaleX;
		dotsY[i] = (dotsY[i] - defaultOffsetY + (offsetY / 100) * height) * scaleY;
	}
	let index = 0;
	return path.replace(/(-?\d+\.\d\d\,-?\d+\.\d\d)/g, dot => {
		let result = `${dotsX[index].toFixed(2)},${dotsY[index].toFixed(2)}`;
		index++;
		return result;
	});

}

export {
	getCarPath,
	getCarPathMobile,
	getBackgroundShapes,
	getMobileBackgroundShapes
};

