'use strict';
import progressBarCtrl from './progress-bar/progress-bar.controller';
import progressBar from './progress-bar/progress-bar.directive';

(function () {

	var $ = window.$;
	var angular = window.angular;

	var scripts = document.getElementsByTagName('script');
	var currentScriptPath = scripts[scripts.length - 1].src;
	var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1);
	var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);

	angular
		.module('superCrate')
		.directive('superCrateForm', superCrateForm)
		.directive('superCrateHeader', superCrateHeader)
		.directive('extraServicesBox', extraServicesBox)
		.directive('paymentBox', paymentBox)
		.directive('zipcode', processZipCode)
		.directive('autcomplete', addressAutocomplete)
		.directive('dropautcomplete', dropaddressAutocomplete)
		.directive('sfisemail', sfIsEmail)
		.directive('validatephone', validatePhone)
		.directive('ccCreditCard', ccCreditCard)
		//.controller('progressBarCtrl', progressBarCtrl)
		//.directive('progressBar', progressBar);

	function extraServicesBox() {

		var directive = {
			templateUrl: relativePath + '/template/extra_services_box.html',
			restrict: 'E'
		};

		return directive;
	}

	function paymentBox() {

		var directive = {
			templateUrl: relativePath + '/template/payment_dialog.html',
			restrict: 'E'
		};

		return directive;
	}


	// SUPERCRATE HEADER
	function superCrateHeader() {

		var directive = {
			controller: superCrateHeaderController,
			templateUrl: relativePath + '/template/superCrateHeader.html',
			restrict: 'E'
		};

		return directive;

		superCrateHeaderController.$inject = ['$scope'];
		function superCrateHeaderController($scope) {

			$scope.total = 0;

			$scope.$on('cart-total', function (event, args) {

				$scope.total = args.total;
				// do what you want to do

			});

			$scope.srcollToForm = function () {

				var destination = $('#supercrateform').offset().top - 120;
				$('body').animate({
					scrollTop: destination
				}, 1100);
				$('html').animate({
					scrollTop: destination
				}, 1100);

			};

			$scope.summarytoggle = function () {

				$scope.mobile_summary_step = !$scope.mobile_summary_step;

			};


		}
	}


	// SUPERCRATE FORM

	function superCrateForm() {

		var directive = {
			controller: superCrateFormController,
			template: require('./superCrateForm.html'),
			restrict: 'E'
		};

		return directive;

		superCrateFormController.$inject = ['$scope', '$rootScope', '$timeout', 'MoveRequestServices', 'config'];

		function superCrateFormController($scope, $rootScope, $timeout, MoveRequestServices, config) {


			function initFlexSliders() {
				$('.flexslider').flexslider({
					animation: 'slide',
					//  itemWidth: 300,
					//  itemMargin: 10,
					//   minItems: 2,
					//  maxItems: 4
					slideshow: false,
					animationLoop: false,
					controlsContainer: '.the_slider .contain'
				});
			}
			var twoDaysMs = 86400000;

			$scope.firstStepDone = false;
			$scope.secondStepDone = false;
			$scope.thirdStepDone = false;
			$scope.client_validation = false;
			$scope.final_validation = false;

			$scope.payment_step = false;

			$scope.threesteps = true;
			$scope.sizeChoosen = false;
			$scope.currentStep = 'step1';
			$scope.nextStep = false;
			$scope.step2img = config.assets + 'supercrate/assets/locationoff.png'; // Location
			$scope.step3img = config.assets + 'supercrate/assets/scheduleoff.png'; // Schedule
			$scope.step4img = config.assets + 'supercrate/assets/extrasuppliesoff.png'; // Extra Supplies
			$scope.step5img = config.assets + 'supercrate/assets/additionaloff.png'; // Extra Services
			$scope.step6img = config.assets + 'supercrate/assets/completeoff.png'; // Last Step
			$scope.currentStepDesription = 'Select Your Package';
			$scope.loading = false;
			$scope.errorValidation = false;
			$scope.firstTimeSubmition = false;
			$scope.invoice_hash = 0;
			$scope.dropOffTime_array = [];
			$scope.promoDiscount = 0;
			$scope.discountAmount = 0;

			$scope.dropOffTime_array[1] = {
				'start': '09:00 AM',
				'finish': '12:00 PM'
			};
			$scope.dropOffTime_array[2] = {
				'start': '12:00 PM',
				'finish': '03:00 PM'
			};
			$scope.dropOffTime_array[3] = {
				'start': '03:00 PM',
				'finish': '06:00 PM'
			};
			$scope.dropOffTime_array[4] = {
				'start': '12:00 PM',
				'finish': '06:00 PM'
			};

			$scope.validation_array = {
				'payment_month': false,
				'payment_year': false,
				'payment_card': false,
				'client_first_name': false,
				'client_last_name': false,
				'client_phone': false,
				'client_email': false,
				'card_number': false,
				'checkout_clicked': false,
				'cvv': false

			};

			$scope.PickupDatePickerOptions = {
				min: '',
				disable: [],
				calendar: {},
				package: {extra_week_cost: 0}
			};
			$scope.pickupDateValidate = false;

			$scope.extra_delivery_rate = 25;

			$scope.order = {
				'package': {},
				'zip_dropoff': '',
				'zip_pickup': '',
				'isdrop': false,
				'ispick': false,
				'need_moving': false,
				'need_storage': false,
				'extra_supplies': [],
				'payment': {},
				'extra_pickup_distance_charge': 0,
				'extra_dropoff_distance_charge': 0,
				'extraweek_number': 0,
				'extraweek': false,
				'discount_amount': 0,
				'dropOffTime_array': $scope.dropOffTime_array
			};
			// Packages
			$scope.packages = [
				{
					id: '1',
					package_name: 'Studio',
					rate: 99,
					number_of_crates: 15,
					extra_week_cost: 15,
					week_of_usage: 2,
					rolling_carts: 1,
					chosen: false
				},
				{
					id: '2',
					package_name: '1-2 BEDROOM',
					rate: 129,
					number_of_crates: 30,
					extra_week_cost: 30,
					week_of_usage: 2,
					rolling_carts: 2,
					chosen: false
				}
				,
				{
					id: '3',
					package_name: '3 BEDROOM',
					rate: 179,
					number_of_crates: 50,
					extra_week_cost: 50,
					week_of_usage: 2,
					rolling_carts: 3,
					chosen: false
				}
				,
				{
					id: '4',
					package_name: '4+ BEDROOM',
					rate: 249,
					number_of_crates: 70,
					extra_week_cost: 70,
					week_of_usage: 2,
					rolling_carts: 7,
					chosen: false
				}

			];

			$scope.floorRates = [
				{
					'floor': '1',
					'rate': 0
				},
				{
					'floor': '2',
					'rate': 15
				},
				{
					'floor': '3',
					'rate': 30
				},
				{
					'floor': '4',
					'rate': 45
				},
				{
					'floor': '5',
					'rate': 45
				}
			];


			// Supplies
			$scope.extra_supplies = [
				{
					supply_name: 'Extra Bin',
					img_src: config.assets + 'supercrate/assets/supplies/extra_crate.png',
					supply_info: 'Per 2 week',
					rate: 1,
					count: 0
				}
				,
				{
					supply_name: 'Extra Dollies',
					img_src: config.assets + 'supercrate/assets/supplies/dollies.jpg',
					supply_info: 'Per 2 weeks',
					rate: 5,
					count: 0
				}
				,
				{
					supply_name: 'Small Box',
					supply_info: 'Size : 21″ x 18″ x 46″',
					img_src: config.assets + 'supercrate/assets/supplies/small_box.jpg',
					rate: 1.99,
					count: 0
				}
				,
				{
					supply_name: 'Medium Box',
					supply_info: 'Size : 18″ x 18″ x 16″',
					img_src: config.assets + 'supercrate/assets/supplies/medium_box.jpg',
					rate: 2.49,
					count: 0
				}
				,
				{
					supply_name: 'Large Box',
					supply_info: 'Size : 18″ x 18″ x 24″',
					img_src: config.assets + 'supercrate/assets/supplies/large-box.jpg',
					rate: 2.99,
					count: 0
				}
				,
				{
					supply_name: 'Cardboard Wardrobe Box',
					supply_info: 'Size : 21″ x 18″ x 46″',
					img_src: config.assets + 'supercrate/assets/supplies/wardrobebox.jpg',
					rate: 10.00,
					count: 0
				}
				,
				{
					supply_name: 'Picture Box',
					supply_info: 'Size : 37″ x 4″ x 27″.',
					img_src: config.assets + 'supercrate/assets/supplies/picture-box-super-crate.jpg',
					rate: 4.00,
					count: 0
				}
				,
				{
					supply_name: 'Packing Paper (bundle)',
					supply_info: ' 16 lbs 1/2 bundle 220 sheets',
					img_src: config.assets + 'supercrate/assets/supplies/packing-paper-super-crate.jpg',
					rate: 30.00,
					count: 0
				}
				,
				{
					supply_name: 'Packing Paper ( 1/2 bundle) ',
					supply_info: '16 lbs 1/2 bundle 220 sheets',
					img_src: config.assets + 'supercrate/assets/supplies/packing-paper-super-crate.jpg',
					rate: 15.00,
					count: 0
				}
				,
				{
					supply_name: 'Mattress Cover, Queen Size',
					supply_info: '96″ x 73″ x 14″',
					img_src: config.assets + 'supercrate/assets/supplies/queen-mattress-bag-super-crate.jpg',
					rate: 10,
					count: 0
				}
				,
				{
					supply_name: 'Mattress Cover, King Size',
					supply_info: ' 100″ x 91″ x 14″ ',
					img_src: config.assets + 'supercrate/assets/supplies/mattress-cover-super-crate.jpg',
					rate: 10,
					count: 0
				}
				,
				{
					supply_name: 'Plastic Stretch Wrap',
					supply_info: ' 18″ x 1,500′ ',
					img_src: config.assets + 'supercrate/assets/supplies/stretch-wrap-super-crate.png',
					rate: 35,
					count: 0
				}
				,
				{
					supply_name: 'Tape, Packing per roll ',
					supply_info: ' 2″ x 55 yds. ',
					img_src: config.assets + 'supercrate/assets/supplies/tape-packing-super-crate.jpg',
					rate: 3.5,
					count: 0
				}
				,
				{
					supply_name: 'TV Box',
					supply_info: 'Can fit a TV up to 60″ in screen diameter',
					img_src: config.assets + 'supercrate/assets/supplies/tv-box-super-crate.jpeg',
					rate: 45,
					count: 0
				}
				,
				{
					supply_name: 'Furniture Pads',
					supply_info: ' 72″ x 0.2″ x 80″ ',
					img_src: config.assets + 'supercrate/assets/supplies/moving-blanket-super-crate.jpg',
					rate: 12,
					count: 0
				}
			];


			$scope.getOrderTotal = function () {
				var total = 0;
				total += $scope.order.package.rate;
				//need extra for Distance
				total += $scope.order.extra_pickup_distance_charge;
				total += $scope.order.extra_dropoff_distance_charge;

				// need extra for FLOOR
				if (angular.isDefined($scope.order.pickupFloor)) {
					total += $scope.floorRates[$scope.order.pickupFloor - 1].rate;
				}
				if (angular.isDefined($scope.order.dropoffFloor)) {
					total += $scope.floorRates[$scope.order.dropoffFloor - 1].rate;
				}

				if ($scope.order.extraweek) {
					total += $scope.order.extraweek_number * $scope.order.package.extra_week_cost;
				}


				// need extra for FLOOR
				angular.forEach($scope.order.extra_supplies, function (charge, key) {
					total += charge.count * charge.rate;
				});

				if ($scope.promoDiscount) {
					$scope.discountAmount = total * $scope.promoDiscount / 100;
					$scope.order.discount_amount = $scope.discountAmount;
					total = total - $scope.discountAmount;
				}

				var grandTotal = total.toFixed(2);

				$rootScope.$broadcast('cart-total', {total: grandTotal});

				return grandTotal;

			};

			function createSuppliesArray() {

				$scope.order.extra_supplies_array = [];

				angular.forEach($scope.order.extra_supplies, function (charge, key) {
					$scope.order.extra_supplies_array.push(charge);
				});

			}

			$scope.add_supply = function (id) {

				$scope.extra_supplies[id].count++;

				$scope.order.extra_supplies[id] = $scope.extra_supplies[id];


				createSuppliesArray();

			};
			$scope.remove_supply = function (id) {

				$scope.extra_supplies[id].count--;

				$scope.order.extra_supplies[id] = $scope.extra_supplies[id];

				createSuppliesArray();

			};
			//chose step1
			// add choosen package to order array
			// higlited choosen pacakge if the return to see
			//
			$scope.choosePackage = function (package_id) {
				$scope.firstStepDone = true;
				$scope.showStep('step2');
				$scope.order.package = $scope.packages[package_id - 1];

				$timeout(function () {

					//if rechoose package un check another one
					angular.forEach($scope.packages, function (value, key) {
						$scope.packages[key].chosen = false;
					});

					//then choose package
					$scope.packages[package_id - 1].chosen = true;
				}, 500);


			};

			$scope.chooseExtraService = function (service) {

				if (service == 'moving') {
					$scope.order.need_moving = !$scope.order.need_moving;
				}
				if (service == 'storage') {
					$scope.order.need_storage = !$scope.order.need_storage;
				}

			};


			$scope.isValidZipCode = function () {

				if (angular.isDefined($scope.order.addressTo
					&& angular.isDefined($scope.order.addressFrom))) {

					return true;
				}
				else {
					return false;
				}

			};

			$scope.isValidAddresses = function () {

				if (angular.isDefined($scope.order.addressTo
					&& angular.isDefined($scope.order.addressFrom))) {

					return true;
				}
				else {
					return false;
				}

			};

			$scope.stepsDesription = [
				{
					'id': 'step1',
					'desription': 'Select Your Package',
					'img': 'Optional caption'
				},
				{
					'id': 'step2',
					'desription': 'Drop Off and Pick Up Location',
					'iconoff': config.assets + 'supercrate/assets/locationoff.png',
					'iconon': config.assets + 'supercrate/assets/locationon.png'
				},
				{
					'id': 'step3',
					'desription': 'Select Your Dates and Times',
					'iconoff': config.assets + 'supercrate/assets/scheduleoff.png',
					'iconon': config.assets + 'supercrate/assets/scheduleon.png'
				}
				,
				{
					'id': 'step4',
					'desription': 'Select Supplies(optional)',
					'iconoff': config.assets + 'supercrate/assets/extrasuppliesoff.png',
					'iconon': config.assets + 'supercrate/assets/extrasupplieson.png'
				},

				{
					'id': 'step5',
					'desription': 'Additional services',
					'iconoff': config.assets + 'supercrate/assets/additionaloff.png',
					'iconon': config.assets + 'supercrate/assets/additionalon.png'
				}
				,
				{
					'id': 'step6',
					'desription': 'Complete Your Order',
					'iconoff': config.assets + 'supercrate/assets/completeoff.png',
					'iconon': config.assets + 'supercrate/assets/completeon.png'
				}
			];


			$scope.step1 = true;
			$scope.step2 = false;
			$scope.step3 = false;
			$scope.step4 = false;
			$scope.step5 = false;
			$scope.step6 = false;
			$scope.mobile_summary_step = true;

			$scope.validateDate = false;
			$scope.validateTime = false;

			$scope.validatePayment = function (type) {

				var current_year = moment().format('YY');

				if (type == 'month') {

					if (angular.isDefined($scope.order.payment.expmonth)) {
						if ($scope.order.payment.expmonth > 0 &&
							$scope.order.payment.expmonth <= 12) {
							return true;
						}
					}
				}
				if (type == 'year') {

					if (angular.isDefined($scope.order.payment.expyear)) {
						if ($scope.order.payment.expyear >= current_year) {
							return true;
						}
					}
				}
				if (type == 'card') {

					if (angular.isDefined($scope.order.payment.credit_card)) {
						var card_type = MoveRequestServices.GetCardType($scope.order.payment.credit_card);
						if (card_type) {
							return true;
						}
					}
				}
				if (type == 'cvv') {


					if (angular.isDefined($scope.order.payment.credit_card) &&
						angular.isDefined($scope.order.payment.cvv)) {

						var card_type = MoveRequestServices.GetCardType($scope.order.payment.credit_card);

						if (card_type == 'AMEX' && $scope.order.payment.cvv.length == 4) {
							return true;
						}
						else if (card_type != 'AMEX' && $scope.order.payment.cvv.length == 3) {
							return true;
						}

					}


				}


				return false;

			};


			//final validation

			$scope.clientValidation = function () {
				var client_validation = false;
				$scope.validation_array.client_first_name = angular.isDefined($scope.order.client_first_name);
				$scope.validation_array.client_last_name = angular.isDefined($scope.order.client_last_name);
				$scope.validation_array.client_email = angular.isDefined($scope.order.client_email);
				$scope.validation_array.client_phone = angular.isDefined($scope.order.client_phone);
				// var firstName ,lastname, email, numbverc, css exist;

				if ($scope.validation_array.client_first_name &&
					$scope.validation_array.client_last_name && $scope.validation_array.client_email && $scope.validation_array.client_phone) {
					$scope.client_validation = true;
					client_validation = true;
				}
				else {
					$scope.client_validation = false;
					client_validation = false;
				}

				return client_validation;

			};
			//final validation

			$scope.finalValidation = function () {
				var final_validation = false;

				//check_fio and payments
				$scope.validation_array.payment_month = $scope.validatePayment('month');
				$scope.validation_array.payment_year = $scope.validatePayment('year');
				$scope.validation_array.card_number = $scope.validatePayment('card');
				$scope.validation_array.cvv = $scope.validatePayment('cvv');
				// var firstName ,lastname, email, numbverc, css exist;


				if ($scope.validation_array.payment_month
					&& $scope.validation_array.payment_year &&
					$scope.validation_array.card_number && $scope.validation_array.client_first_name &&
					$scope.validation_array.client_last_name
				) {
					final_validation = true;
				}

				$scope.final_validation = final_validation;

				return final_validation;

			};

			$scope.applypromocode = function () {

				// check if definde order.promocode
				var promo = 'lexel';
				var promo2 = 'www';

				//check if valide order.promocode
				if ($scope.order.promocode == promo || $scope.order.promocode == promo2) {
					$scope.promoDiscount = 10;
					// apply promocode discount

					//disable promoblock

				}
				else {
					//if false show message
					swal({
						title: 'The code has been entered incorrectly.',
						text: 'Whoops! The code are case sensitive so enter it exactly as it is written and try again.',
						icon: 'warning',
						button: 'Ok'
					});
				}


			};

			function validateSteps(step) {

				var validation = false;
				$scope.validate_msg = '';

				switch (step) {
				case 'step1':
					if ($scope.firstStepDone) {
						validation = true;
					}
					break;
				case 'step2':
					if ($scope.firstStepDone) {
						validation = true;
					}
					break;
				case 'step3':
					$scope.step3 = false;
					if ($scope.firstStepDone) {
						validation = true;
					}
					break;
				case 'step4':
					$scope.step4 = false;
					if ($scope.firstStepDone && $scope.thirdStepDone) {
						validation = true;
					}
					break;
				case 'step5':
					$scope.step5 = false;
					if ($scope.firstStepDone && $scope.thirdStepDone) {
						validation = true;
					}
					break;
				case 'step6':
					$scope.step6 = false;
					if ($scope.firstStepDone && $scope.thirdStepDone) {
						validation = true;
					}
					break;
				case 'stepSuccess':
					if ($scope.firstStepDone && $scope.thirdStepDone) {
						validation = true;
					}
					break;

				default:

				}

				$scope.errorValidation = !validation;

				return validation;
			}

			/*
			function validateSteps(step){

				 var validation = false;
				 $scope.validate_msg = '';

				 switch (step) {
					  case 'step1':
							if($scope.firstStepDone){
								 validation = true;
							}
							break;
					  case 'step2':
							if($scope.firstStepDone){
								 validation = true;
							}
							break;
					  case 'step3':
							$scope.step3 = false;
							if($scope.firstStepDone && $scope.secondStepDone){
								 validation = true;
							}
							break;
					  case 'step4':
							$scope.step4 = false;
							if($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone){
								 validation = true;
							}
							break;
					  case 'step5':
							$scope.step5 = false;
							if($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone){
								 validation = true;
							}
							break;
					  case 'step6':
							$scope.step6 = false;
							if($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone){
								 validation = true;
							}
							break;
					  case 'stepSuccess':
							if($scope.firstStepDone && $scope.secondStepDone && $scope.thirdStepDone){
								 validation = true;
							}
							break;

					  default:

				 }

				 $scope.errorValidation = !validation;

				 return validation;
			}
			*/

			$scope.showStep = function (new_step) {

				if (new_step != $scope.currentStep) {


					if (validateSteps(new_step)) {
						nextStep(new_step);
						hideCurrentStep(new_step);
						showStepDesription(new_step);
					}
				}

			};

			$scope.dropoffdateChange = function () {

				if (angular.isDefined($scope.order.dropoffDate)) {

					$scope.validateDate = true;
					var tempdate = $scope.order.dropoffDate;
					var StorageDay = new Date(Date.parse(tempdate) + twoDaysMs);
					$scope.calendar = {};
					var year = moment($scope.order.dropoffDate).format('YYYY');
					$scope.calendar[year] = {};
					for (var i = 0; i < 14; i++) {

						var date = moment(tempdate).add(1, 'd').format('YYYY-MM-DD');
						$scope.calendar[year][date] = '1';
						tempdate = date;
					}
					for (var i = 0; i < 305; i++) {

						var date = moment(tempdate).add(1, 'd').format('YYYY-MM-DD');
						$scope.calendar[year][date] = '2';
						tempdate = date;
					}
					$scope.PickupDatePickerOptions.calendar = $scope.calendar;
					$scope.PickupDatePickerOptions.min = StorageDay;
					$scope.PickupDatePickerOptions.package = $scope.order.package;
					// $scope.PickupDatePickerOptions.disable = [1];
					// $scope.order.pickupDate = moment($scope.order.dropoffDate).add(2, 'w').format('MMMM DD,YYYY');


					// $scope.note_text = "We add 2 weeks to Pickup Date. You can change it if you want."

					// Create Calendar

				}
			};

			$scope.pickupdateChange = function () {

				//check if more then 2 week adde extra charge for extra week.
				var a = moment($scope.order.dropoffDate);
				var b = moment($scope.order.pickupDate);
				var diff = b.diff(a, 'days');
				if (diff > 14) {
					//apply extra rate
					$scope.order.extraweek = true;
					var weeks = Math.floor(diff / 14);
					$scope.order.extraweek_number = weeks;
				}
				else {

					$scope.order.extraweek = false;
				}

				$scope.pickupDateValidate = true;

				//initial rate, calendar update 2 times
				$scope.extra_supplies[0].rate = 1;
				$scope.extra_supplies[1].rate = 5;


				//update Extra Supplies
				if ($scope.order.extraweek_number > 0) {
					$scope.extra_supplies[0].rate = $scope.extra_supplies[0].rate * $scope.order.extraweek_number;
					$scope.extra_supplies[1].rate = $scope.extra_supplies[1].rate * $scope.order.extraweek_number;

					$scope.extra_supplies[0].supply_info = 'Per ' + ($scope.order.extraweek_number + 2) + ' week';
					$scope.extra_supplies[1].supply_info = 'Per ' + ($scope.order.extraweek_number + 2) + ' week';

				}
				else {
					$scope.extra_supplies[0].rate = $scope.extra_supplies[0].rate * 2;
					$scope.extra_supplies[1].rate = $scope.extra_supplies[1].rate * 2;
				}
			};


			$scope.TimeChange = function () {


				if (angular.isDefined($scope.order.dropoffTime)) {
					$scope.validateTime = true;
				}
				;

			};

			// Check if step is complete, if true color progress bar in green if even not current step
			$scope.isCompleteStep = function (step) {

				switch (step) {
				case 'step1':
					$scope.step1 = false;
					break;
				case 'step2':
					$scope.step2 = false;
					break;
				case 'step3':
					$scope.step3 = false;
					break;
				case 'step4':
					$scope.step4 = false;
					break;
				case 'step5':
					$scope.step5 = false;
					break;
				case 'step6':
					$scope.step6 = false;
					break;

				default:

				}

			};

			function showStepDesription(step) {

				angular.forEach($scope.stepsDesription, function (value, key) {

					if (angular.equals(value.id, step)) {
						$scope.currentStepDesription = value.desription;
					}

				});

			}


			$scope.validateStep = function (step) {
				var validate = false;
				if (step == 'step3') {
					if (angular.isDefined($scope.order.dropoffTime)
						&& angular.isDefined($scope.order.dropoffDate)
						&& angular.isDefined($scope.order.dropoffFloor)
						&& angular.isDefined($scope.order.pickupFloor)
					) {
						validate = true;
						$scope.note_text = 'Everything looks good. You can continue to next step.';
						$scope.thirdStepDone = true;

					}
					else {
						$scope.thirdStepDone = false;
					}
				}

				var order = $scope.order;
				return validate;
			};


			$scope.showPayment = function () {

				$scope.payment_step = true;

			};

			// CHECKOUT FUNCTION //


			$scope.submitOrder = function () {

				//1. validate order details field
				$scope.validation_array.checkout_clicked = true;
				var clientValidation = $scope.clientValidation();

				if (clientValidation) {

					//2. Prepare array from order and create request
					var invoice_array = MoveRequestServices.createInvoice($scope.order, $scope.floorRates);
					invoice_array.total = $scope.getOrderTotal();
					invoice_array.totalInvoice = $scope.getOrderTotal();

					$scope.order.invoice = invoice_array;
					$scope.order.toStorage = false;
					$scope.order.notes = MoveRequestServices.createSalesNote($scope.order, $scope.floorRates,
						$scope.getOrderTotal(), false);
					$scope.order.client_notes = MoveRequestServices.createSalesNote($scope.order, $scope.floorRates,
						$scope.getOrderTotal(), true);
					var request1 = MoveRequestServices.PrepareRequestArray($scope.order);
					if (angular.isUndefined($scope.order.pickupDate)) {
						$scope.order.pickupDate = $scope.order.dropoffDate;
					}
					$scope.order.dropoffDate = $scope.order.pickupDate;
					$scope.order.dropoffTime = '4'; // set time from 12pm to 6pm;
					$scope.order.toStorage = true;
					var request2 = MoveRequestServices.PrepareRequestArray($scope.order);


					var two_requests = {
						'storage1': request1,
						'storage2': request2,
						'crate_sales_notes': $scope.order.notes,
						'crate_client_notes': $scope.order.client_notes,
						'crate_invoice': invoice_array
					};


					$scope.payment_step = true;
					var promise = MoveRequestServices.CreateRequest(two_requests);
					promise.then(function (data) {
						var hash = data.hash;
						$scope.invoice_hash = hash;
						$scope.firstTimeSubmition = true;


						//SHOW PAYMENT WINDOW

					}, function (reason) {

						$scope.loading = false;
						swal({
							title: 'Error',
							text: 'Problem with creating order.Please call in the office',
							icon: 'warning',
							button: 'Ok'
						});
						// show payment dialog agagin
					});

				}
			};

			$scope.checkout = function () {

				// $scope.order.notes =
				// MoveRequestServices.createSalesNote($scope.order,$scope.floorRates,$scope.getOrderTotal());

				$scope.validation_array.checkout_clicked = true;
				//1. validate payments field
				var finalValidation = $scope.finalValidation();

				if (finalValidation) {

					if ($scope.firstTimeSubmition) {
						$scope.loading = true;
						MoveRequestServices.hashInvoice($scope.invoice_hash).then(function (response) {
							var invoice = response.data;
							var invoiceId = response.id;
							invoice.id = response.id;
							//show error

							var payment_info = MoveRequestServices.createPaymentInfo($scope.order, invoice);

							var paymentPromise = MoveRequestServices.sendPayment(payment_info, invoice.entity_id, 0);
							paymentPromise.then(function (data) {

								$scope.loading = false;

								//false
								if (!data.status) {
									swal({
										title: 'Error',
										text: data.text,
										icon: 'warning',
										button: 'Ok'
									});
									//show payment window only

								}
								else {
									//show success page
									$scope.showStep('stepSuccess');
								}

							}, function (reason) {
								var reason = reason;
								$scope.loading = false;
								// show payment dialog agagin
							});

						});
					}

				}


			};

			$scope.summarytoggle = function () {

				$scope.mobile_summary_step = !$scope.mobile_summary_step;

			};


			$scope.toggleImage = function (step) {

				switch (step) {
				case 'step1':

					$scope.step2img = config.assets + 'supercrate/assets/locationoff.png';
					$scope.step3img = $scope.stepsDesription[2].iconoff;
					$scope.step4img = $scope.stepsDesription[3].iconoff;
					$scope.step5img = $scope.stepsDesription[4].iconoff;
					$scope.step6img = $scope.stepsDesription[5].iconoff;

					break;
				case 'step2':
					if ($scope.step2) {
						$scope.step2img = config.assets + 'supercrate/assets/locationon.png';
					}
					else {
						$scope.step2img = config.assets + 'supercrate/assets/locationoff.png';
					}

					$scope.step3img = $scope.stepsDesription[2].iconoff;
					$scope.step4img = $scope.stepsDesription[3].iconoff;
					$scope.step5img = $scope.stepsDesription[4].iconoff;
					$scope.step6img = $scope.stepsDesription[5].iconoff;

					break;
				case 'step3':
					if ($scope.step3) {
						$scope.step3img = $scope.stepsDesription[2].iconon;
					}
					else {
						$scope.step3img = $scope.stepsDesription[2].iconoff;
					}

					$scope.step4img = $scope.stepsDesription[3].iconoff;
					$scope.step5img = $scope.stepsDesription[4].iconoff;
					$scope.step6img = $scope.stepsDesription[5].iconoff;

					break;
				case 'step4':
					if ($scope.step4) {
						$scope.step4img = $scope.stepsDesription[3].iconon;
					}
					else {
						$scope.step4img = $scope.stepsDesription[3].iconoff;
					}


					$scope.step5img = $scope.stepsDesription[4].iconoff;
					$scope.step6img = $scope.stepsDesription[5].iconoff;
					break;
				case 'step5':
					if ($scope.step5) {
						$scope.step5img = $scope.stepsDesription[4].iconon;
					}
					else {
						$scope.step5img = $scope.stepsDesription[4].iconoff;
					}

					$scope.step6img = $scope.stepsDesription[5].iconoff;

					break;
				case 'step6':
					if ($scope.step6) {
						$scope.step6img = $scope.stepsDesription[5].iconon;
					}
					else {
						$scope.step6img = $scope.stepsDesription[5].iconoff;
					}
					break;
				default:

				}


			};

			function hideCurrentStep(step) {

				switch ($scope.currentStep) {
				case 'step1':
					$scope.step1 = false;
					break;
				case 'step2':
					$scope.step2 = false;
					break;
				case 'step3':
					$scope.step3 = false;
					break;
				case 'step4':
					$scope.step4 = false;
					break;
				case 'step5':
					$scope.step5 = false;
					break;
				case 'step6':
					$scope.step6 = false;
					break;
				default:

				}
				$scope.currentStep = step;

			}

			function nextStep(step) {
				switch (step) {
				case 'step1':
					$scope.step1 = true;
					break;
				case 'step2':
					$scope.step2 = true;
					break;
				case 'step3':
					$scope.step3 = true;
					break;
				case 'step4':
					$scope.step4 = true;
					initFlexSliders();
					break;
				case 'step5':
					$scope.step5 = true;
					break;
				case 'step6':
					$scope.step6 = true;
					break;
				case 'stepSuccess':
					$scope.stepSuccess = true;
					break;

				default:

				}
				//change progressbar image
				$scope.toggleImage(step);
			}


		}
	}


	// Zip Code Directive
	processZipCode.$inject = ['$rootScope', '$http', 'superCrateServices'];

	function processZipCode($rootScope, $http, superCrateServices) {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function (scope, element, attrs) {
				element.bind('change', function () {
					if (element.val().length != 5) {
						element.addClass('error');
						element.val('');
					}
				});

				scope.$watch(attrs.ngModel, function (zip_code) {

					if (!angular.isUndefined(zip_code)) {
						element.removeClass('success');

						if (zip_code.length == 5) {
							element.removeClass('error');

							var geocoder = new google.maps.Geocoder();
							var type = attrs.ngModel;

							geocoder.geocode({
								'address': zip_code,
								'country': 'US'
							}, function (results, status) {
								if (status == google.maps.GeocoderStatus.OK) {
									var arrAddress = results[0]['address_components'];
									var address = superCrateServices.parseZipToAddress(arrAddress, type);

									//1. Get if it's in the pologin
									var inLocation = superCrateServices.ContainsLocation(results);


									if (type == 'order.zip_dropoff') {

										scope.order.addressFrom = address;
										scope.order.extraDistanceDropoff = inLocation;
										//Check if we delivery
										//If not show validatio and message
										//If yes and we charge show message with extra charge
										scope.dropoff_msg = 'We have extra fee for delivery $20';
										element.addClass('success');
									}
									else {
										scope.order.addressTo = address;
										scope.order.extraDistancePickup = inLocation;
										//If not show validatio and message
										//If yes and we charge show message with extra charge
										scope.pickup_msg = 'We have extra fee for pickup $20';
										element.addClass('success');
									}

									//INIT FIELDS
									if (!angular.isUndefined(address.state) && address.country == 'US') {

										// showAddress(address, type)
									}
									else {

										if (type == 'request.zipFrom') {
											angular.element('#calc-info-steps .box_info .moving-from').empty().append(
												'<h3>Moving From:</h3><span class="error_alarm">We do not service zip ' + address.postal_code + '.</span>').show(
												'slow');
										} else {
											angular.element('#calc-info-steps .box_info .moving-to').empty().append(
												'<h3>Moving To:</h3><span class="error_alarm">We do not service zip ' + address.postal_code + '.</span>').show(
												'slow');
										}

										element.addClass('error');
										element.val('');
										element.removeClass('success');
									}
								} else {
									if (type == 'request.zipFrom') {
										angular.element('#calc-info-steps .box_info .moving-from').empty().append(
											'<h3>Moving From:</h3><span class="error_alarm">Not found zip code ' + zip_code + '.</span>').show(
											'slow');
									} else {
										angular.element('#calc-info-steps .box_info .moving-to').empty().append(
											'<h3>Moving To:</h3><span class="error_alarm">Not found zip code ' + zip_code + '.</span>').show(
											'slow');
									}

									element.addClass('error');
									element.val('');
									element.removeClass('success');
								}
							});
						}
					}
				});
			}
		};
	};


	dropaddressAutocomplete.$inject = ['superCrateServices'];

	function dropaddressAutocomplete(superCrateServices) {
		return {
			require: 'ngModel',
			link: function (scope, element) {
				var options = {
					types: [],
					componentRestrictions: {country: 'us'}
				};
				scope.gPlace2 = new google.maps.places.Autocomplete(element[0], options);

				google.maps.event.addListener(scope.gPlace2, 'place_changed', function () {

					var placeResult = scope.gPlace2.getPlace();
					var addressComponents = placeResult.address_components;
					var street = '';
					var number = '';
					var postal_code = '';

					var address = superCrateServices.parseFullTAddress(placeResult);
					//1. Get if it's in the pologin
					var inLocation = superCrateServices.ContainsLocation(placeResult.geometry);

					var street_validate = false;
					//contains undefined in street or postal code
					if (address.full_adr.indexOf('undefined') == -1) {
						street_validate = true; // didn't find undefined we good
					}
					else {
						inLocation.ifmove = false;
					}


					scope.$apply(function () {
						if (inLocation.ifmove) {
							scope.dropoff_msg = '';
							element.val(address.full_adr);
							element.addClass('success');
							element.removeClass('error');
							element.addClass('desktopzip');
							scope.order.dropoffLocation = address;
							//autopopuate billing address and zip
							scope.order.billing_zip = address.postal_code;
							scope.order.billing_adr = address.street;
							scope.order.isdrop = true;

							if (scope.order.isdrop && scope.order.ispick) {
								scope.secondStepDone = true;
							}

							if (inLocation.ifextra) {
								scope.order.extra_dropoff_distance_charge = scope.extra_delivery_rate;
								scope.dropoff_msg = 'We have extra delivery charge for this address $' + scope.order.extra_dropoff_distance_charge;
							}
							else {
								scope.order.extra_dropoff_distance_charge = 0;
							}

						}
						else {

							element.addClass('error');
							element.val('');
							element.removeClass('success');
							if (!street_validate) {
								scope.dropoff_msg = 'We need full address.';
							}
							else {
								scope.dropoff_msg = 'We don\'t move to this location.';
							}
							scope.order.isdrop = false;

						}

					});


				});
			}
		};
	}


	addressAutocomplete.$inject = ['superCrateServices'];

	function addressAutocomplete(superCrateServices) {
		return {
			require: 'ngModel',
			link: function (scope, element) {
				var options = {
					types: [],
					componentRestrictions: {country: 'us'}
				};
				scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

				google.maps.event.addListener(scope.gPlace, 'place_changed', function () {

					var placeResult = scope.gPlace.getPlace();
					var addressComponents = placeResult.address_components;
					var street = '';
					var number = '';
					var postal_code = '';

					var address = superCrateServices.parseFullTAddress(placeResult);
					//1. Get if it's in the pologin
					var inLocation = superCrateServices.ContainsLocation(placeResult.geometry);

					var street_validate = false;
					//contains undefined in street or postal code
					if (address.full_adr.indexOf('undefined') == -1) {
						street_validate = true; // didn't find undefined we good
					}
					else {
						inLocation.ifmove = false;
					}

					scope.$apply(function () {
						if (inLocation.ifmove) {
							scope.pickup_msg = '';
							element.val(address.full_adr);
							element.addClass('success');
							element.removeClass('error');
							element.addClass('desktopzip');
							scope.order.pickupLocation = address;
							scope.order.ispick = true;

							if (scope.order.isdrop && scope.order.ispick) {
								scope.secondStepDone = true;
							}
							else {
								scope.secondStepDone = false;
							}

							if (inLocation.ifextra) {
								scope.order.extra_pickup_distance_charge = scope.extra_delivery_rate;
								scope.pickup_msg = 'We have extra pickup charge for this address $' + scope.order.extra_pickup_distance_charge;
							}
							else {
								scope.order.extra_pickup_distance_charge = 0;
							}
						}
						else {
							element.addClass('error');
							element.removeClass('success');
							element.val('');
							if (!street_validate) {
								scope.pickup_msg = 'We need full address.';
							}
							else {
								scope.pickup_msg = 'We don\'t move to this location.';
							}
							scope.order.ispick = false;

						}

					});


				});
			}
		};
	}

	function validatePhone() {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function (scope, element, attrs) {
				element.bind('change', function () {
					if (element.val().length < 14) {
						element.addClass('error');
						element.removeClass('success');
						element.removeClass('desktopzip');
						scope.validation_array.client_phone = false;
					}
				});

				scope.$watch(attrs.ngModel, function (nval) {
					if (!angular.isUndefined(nval)) {
						if (element.val().length == 14) {
							element.removeClass('error');
							element.addClass('success');
							element.addClass('desktopzip');
							scope.validation_array.client_phone = true;
						}
					}
				});
			}
		};
	}

	function sfIsEmail() {
		return {
			restrict: 'A',
			link: function (scope, element) {
				element.bind('change', function () {
					if (element.val().length == 0) {
						angular.element('#edit-confirmemail').val('');
					}
					else {
						var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						var ismail = regex.test(element.val());

						if (!ismail) {
							element.removeClass('success');
							element.removeClass('desktopzip');
							element.addClass('error');
							element.val('');
							scope.validation_array.client_email = false;
						}
						else {
							element.removeClass('error');
							element.addClass('success');
							element.addClass('desktopzip');
							scope.validation_array.client_email = true;
						}

					}
				});
			}
		};
	};

	function ccCreditCard() {
		return {
			restrict: 'A',
			scope: {},
			link: function (scope, element) {

				element.bind('change paste keyup', function () {

					var credit_number = element.val();

					var type = GetCardType(credit_number);

					if (credit_number.length >= 15) {

						if (type == 'AMEX') {
							$('#cvc').attr('maxlength', 4);
							if (credit_number.length == 15) {
								element.removeClass('error');
								element.addClass('success');
								element.addClass('desktopzip');
							}

						} else {
							$('#cvc').attr('maxlength', 3);
							if (!type && (credit_number.length == 16)) {
								element.addClass('error');
								element.removeClass('success');
								element.removeClass('desktopzip');
							}
							else {
								element.removeClass('error');
								element.addClass('success');
								element.addClass('desktopzip');
							}
						}

					}
					else {
						element.removeClass('success');
						element.removeClass('desktopzip');
					}


				});

				function GetCardType(number) {
					// visa
					var re = new RegExp('^4');
					if (number.match(re) != null) {
						return 'Visa';
					}

					// Mastercard
					re = new RegExp('^5[1-5]');
					if (number.match(re) != null) {
						return 'Mastercard';
					}

					// AMEX
					re = new RegExp('^3[47]');
					if (number.match(re) != null) {
						return 'AMEX';
					}

					// Discover
					re = new RegExp('^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)');
					if (number.match(re) != null) {
						return 'Discover';
					}

					// Visa Electron
					re = new RegExp('^(4026|417500|4508|4844|491(3|7))');
					if (number.match(re) != null) {
						return 'Visa Electron';
					}

					return '';
				}
			}

		};
	}

})();


/*http-server -o*/

/* https://google-developers.appspot.com/maps/documentation/utils/geojson/  */