'use strict';

const easingFunctions = {
	squareEasing
};

window.angular
	.module('superCrate')
	.controller('animatedNumberCtrl', animatedNumberCtrl);

animatedNumberCtrl.$inject = ['$scope', '$element', '$interval'];

function animatedNumberCtrl($scope, $element, $interval) {
	const easing = $scope.easingFunction
						? $scope.easingFunction
						: $scope.easingName && easingFunctions[$scope.easingName]
						  ? easingFunctions[$scope.easingName]
						  : easingFunctions.squareEasing;
	const delay = 30;
	const changingStateStep = delay / +$scope.duration;

	let changingState = 0;
	let different = $scope.targetNumber;
	let oldNumber = $scope.targetNumber;
	let interval;

	$scope.visibleNumber = 0;

	$scope.$watch('targetNumber', targetChanged);
	function targetChanged(newVal) {
		if (isNaN(newVal)) newVal = 0;
		changingState = 0;
		different = newVal - $scope.visibleNumber;
		oldNumber = $scope.visibleNumber;
		stopInterval();
		interval = $interval(animationStep, delay);
		$element.addClass('changing-value');
	}

	function animationStep() {
		if (changingState >= 1) {
			$scope.visibleNumber = oldNumber + different * 1;
			if (changingState >= 1) stopInterval();
		} else {
			$scope.visibleNumber = oldNumber + different * easing(changingState);
		}
		changingState += changingStateStep;
	}

	function stopInterval() {
		$interval.cancel(interval);
		$element.removeClass('changing-value');
	}
}

/**
 *
 * @param n {number} - 0..1
 * @return {number} - 0..1
 */
function squareEasing(n) {
	let invertedN = 1 - n;
	return 1 - invertedN * invertedN;
}
