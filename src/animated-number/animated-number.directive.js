'use strict';

window.angular.module('superCrate')
	.directive('animatedNumber', [animatedNumber]);

function animatedNumber() {
	return {
		replace: false,
		restrict: 'EA',
		controller: 'animatedNumberCtrl',
		scope: {
			targetNumber: '<',
			visibleNumber: '=',
			easingName: '<',
			easingFunction: '<',
			duration: '<',
		}
	};
}