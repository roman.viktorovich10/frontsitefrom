const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

let defaultPlugins = [
  //new BundleAnalyzerPlugin(),
  new MiniCssExtractPlugin({
		filename: '[name].css'
	}),
	new ProgressBarPlugin(),
	new HtmlWebpackPlugin({
		template: './src/index.html',
		filename: '../bundle/index.html',
		inject: false
	}),
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery',
		_: 'lodash',
	}),
];

module.exports = defaultPlugins;