const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const CSS_LOADERS = [
	'style-loader',
	//MiniCssExtractPlugin.loader,
	{
		loader: 'css-loader',
		options: {minimize: true}
	},
	{
		loader: 'postcss-loader',
		options: {
			sourceMap: true,
			plugins: () => [
				require('autoprefixer')
			]
		}
	},
	'resolve-url-loader',
];

let STYLUS_LOADER = CSS_LOADERS.concat([
	{
		loader: 'stylus-loader'
	}
]);

let SASS_LOADER = CSS_LOADERS.concat([
	{
		loader: 'sass-loader' // compiles Sass to CSS
	}
]);

module.exports = [
	{
		test: require.resolve('snapsvg/dist/snap.svg.js'),
		use: 'imports-loader?this=>window,fix=>module.exports=0',
	},
	{
		test: /\.(js|jsx|mjs)$/,
		exclude: /node_modules/,
		use: {
			loader: 'babel-loader',
		}
	},
	{
		test: /\.jpg$/,
		'use': ['file-loader']
	},
	{
		test: /\.png$/,
		'use': ['url-loader?mimetype=image/png']
	},
	{
		test: /\.gif$/,
		'use': ['url-loader?mimetype=image/gif']
	},
	{
		test: /\.css$/,
		use: CSS_LOADERS
	},
	{
		test: /\.styl$/,
		use: STYLUS_LOADER
	},
	{
		test: /\.(sass|scss)$/,
		use: SASS_LOADER
	},
	{
		test: /\.(eot|ttf|woff|woff2)$/,
		use: [
			{
				loader: 'file-loader'
			}
		]
	},
	{
		test: /\.(svg)$/,
		exclude: /node_modules/,
		use: [
			{
				loader: 'url-loader',
				options: {
					limit: 100000
				}
			}
		]
	},
	{
		test: /\.html$/,
		'use': [
			{
				loader: 'html-loader',
				options: {
					minimize: false
				}
			},
		]
	},
];