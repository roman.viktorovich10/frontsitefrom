const path = require('path');
const DEFAULT_LOADERS = require('./default-loaders');
const DEFAULT_PLUGINS = require('./default-plugins');
const DEFAULT_RESOLVE = require('./default-resolve');
const PUBLIC_PATH = '/';

module.exports = {
	context: global.rootPath,
	watchOptions: {
		aggregateTimeout: 100,
	},
	// devServer: {
	// 	//contentBase: path.join(global.rootPath, 'bundle'),
	// 	compress: true,
	// 	port: 9000,
	// 	historyApiFallback: true,
	// },
	devtool: global.isProduction
				? false
				: 'eval-source-map',
	entry: {
		//'source': path.join(global.rootPath, 'src', 'source.js'),
		//'imports': path.join(global.rootPath, 'src', 'imports.js'),
		//'progress-bar': path.join(global.rootPath, 'src', 'progress-bar', 'index.js'),
		//'animated-number': path.join(global.rootPath, 'src', 'animated-number', 'index.js'),
		'fullBuild': path.join(global.rootPath, 'supercrateBuild', 'index.js'),
	},
	output: {
		filename: '[name].js',
		path: path.resolve(global.rootPath, '../supercrateback/frontSites/sites/demo/wp-content/themes/shopscape/supercrate'),//path.join(global.rootPath, 'bundle'),
		publicPath: PUBLIC_PATH
	},
	module: {
		rules: DEFAULT_LOADERS,
	},
	plugins: DEFAULT_PLUGINS,
	resolve: DEFAULT_RESOLVE,
};