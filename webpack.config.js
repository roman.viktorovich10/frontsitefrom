module.exports = env => {
  console.log(env);
  const CLI_PARAMS = process.argv;
  global.NODE_ENV = env || 'production';
  global.isProduction = global.NODE_ENV === 'production';
  let targets = [];
  global.rootPath = __dirname;

  if (~CLI_PARAMS.indexOf('--dev')) {
    targets.push(require('./webpack_configs/webpack.config.dev'));
  }

  if (~CLI_PARAMS.indexOf('--prod')) {
    targets.push(require('./webpack_configs/webpack.config.prod'));
  }

  return targets;
};